/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';


import React from 'react';
import Navigation from './src/config/navigation';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './src/redux/sagas/sagas';
import rootReducer from './src/redux/reducers';

const middleware = createSagaMiddleware();
const store = createStore(rootReducer, applyMiddleware(middleware) );
middleware.run( rootSaga );


const Root = () => (
	<Provider store={ store } >
		<Navigation />
	</Provider>
);


AppRegistry.registerComponent(appName, () => Root);
