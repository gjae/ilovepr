import React, { Component } from 'react';
import { 
	View,
	StyleSheet
} from 'react-native';

export default class RoundIcon extends Component {

	constructor(props){
		super(props);
		this.state = {
			styles: {}
		}
	}

	componentWillMount(){

		this.setState({
			styles: StyleSheet.create({
				round :{
					backgroundColor: ( typeof this.props.backgroundColor == "undefined") ?  '#F57900' : this.props.backgroundColor,
					width: 55,
					height: 55,
					borderRadius: 40,
					paddingTop: 4,
					paddingLeft: 8.5
				}
			}),
		})
	}

	render(){
		let { styles } = this.state
		return(

			<View  style={[ styles.round ] }>
				{this.props.children}
			</View>
		);

	}

}
