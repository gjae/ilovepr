import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, FlatList, TouchableOpacity, Image, ImageBackground, Dimensions, Platform } from 'react-native';
import { Row, Col, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import { Divider } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';


class EventHeader extends Component {

	constructor(props){
		super(props);
	}


	render(){
		return(
			<View>

			<LinearGradient
				start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}}
				locations={[0,0.5,0.6]}
				colors={['rgba(0,0,0, 1.0)', "rgba(0,0,0, 0.5)", "rgba(0,0,0, 0.5)"]}
			>
				<ImageBackground source={ this.props.source } style={styles.mainView}>
					<Grid style={styles.gridStyle}>
						<Row style={{height: '56.5%'}}>
							<Col style={{ width: '90%' }}>
								<TouchableOpacity onPress={()=> { this.props.navigation.goBack() }}>
									<Icon name="md-arrow-back" color="#ffffff" size={28} />
								</TouchableOpacity>
							</Col>
						</Row>
						<Row>
								<Col>
									<Text style={styles.placeText} >{this.props.navigation.state.params.event.nombre}</Text>
								</Col>
						</Row>
					</Grid>
				</ImageBackground>
			</LinearGradient>
			</View>
		);
	}

}

function mapDispatchToProps(dispatch , ownProps){
	return {
		...ownProps
	}
}

export default connect(null, mapDispatchToProps)(EventHeader);

let { width, height } = Dimensions.get("screen");
const styles= StyleSheet.create({

	mainView :{
		height: 200,
	},	
	background :{
		height:  ( height * 35 ) / 100,
		width: '100%'

	},
	gridStyle :{
		paddingTop: "8%",
		paddingLeft: '3%'
	},
	placeText :{
		color:"#ffffff",
		fontSize: 32,
		fontFamily: 'NeutraText-DemiItalic'
	}

})