import React from 'react';
import { View, StyleSheet} from 'react-native';

export default () => (
    <View style={styles.divider}/>
);

const styles = StyleSheet.create({
    divider: {
        width: '100%',
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#777777',
        marginVertical: 5
    }
});