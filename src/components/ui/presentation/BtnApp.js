import React, { Component } from 'react';
import { TouchableOpacity, Text, View, StyleSheet } from 'react-native';



const BtnApp = (props) => (
	<TouchableOpacity style={ styles.touchableStyles } >
		<View >
			{props.children}
		</View>
	</TouchableOpacity>
);

export default BtnApp;

const styles = StyleSheet.create({

	touchableStyles: {
		width: 200,
		height: 200,
		borderWidth: 0.5,
		borderColor: '#ef5350'
	}

})