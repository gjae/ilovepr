import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native';
import ImageSlider from 'react-native-image-slider';
import Icon from 'react-native-vector-icons/Ionicons';

export default class CardInfo extends React.Component{

    constructor(props){
        super(props);
    }

    _renderSlide(index, item, style, width ){
        return(
            <View key={index} style={[style, styles.customSlide]}>
                <Image source={{ uri: item }} style={styles.customImageSlide} />
            </View>
        )
    }

    render(){
        return(
            <View style={styles.mainView} >
                <View style={{ height: "60%", zIndex: 0 }}>
                    <ImageSlider 
                        loopBothSides
                        autoPlayWithInterval={3000}
                        images={this.props.images}
                        style={{height: "80%"}}
                    />
                </View>
                <View style={{ height: "40%" }}> 
                    {this.props.children}
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    mainView: {
     flex: 1,
     flexDirection: "column" ,
     justifyContent: "center",
     height: 210,
     width: "100%",
     marginBottom: 3.5,
     elevation: 0.8,
     backgroundColor: "#ffffff"
    },
    customSlide : {
        width: "100%",
        height: 75
    },
    customImageSlide: {
        width: "100%",
        height: 75
    }
})