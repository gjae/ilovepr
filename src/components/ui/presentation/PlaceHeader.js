import React, { Component } from 'react';
import { View, ImageBackground, Text, StyleSheet, Dimensions, TouchableOpacity , Alert} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import {
	MARK_PLACE
} from '../../../redux/actions/startScreens'

class PlaceHeader extends Component{


	constructor(props){
		super(props);
		this.state = {
			place: "",
			favoriteIcon: this.props.navigation.state.params.place.marcado == null? "ios-heart-outline" : "ios-heart" 
		}
	}

	componentDidMount(){
		let { place } = this.props.navigation.state.params;
		this.setState({
			...place
		});
	}

	render(){

		return(

			<ImageBackground source={this.props.image} style={styles.background}>
				<Grid style={styles.gridStyle}>
					<Row style={{height: '70%'}}>
						<Col style={{ width: '80%' }}>
							<TouchableOpacity onPress={()=> { this.props.navigation.goBack() }}>
								<Icon name="md-arrow-back" color="#ffffff" size={28} />
							</TouchableOpacity>
						</Col>
						<Col style={{elevation: 1}}>
							<TouchableOpacity onPress={()=>{
								this.props.dispatch({
									type: MARK_PLACE,
									data:{ 
										resource: "places/save",
										is: "favorito",
										to: this.props.navigation.state.params.place.marcado == null ? "mark" : 'dismark',
										lugar_id: this.props.navigation.state.params.place.id	
									}
								});
								if( this.props.navigation.state.params.place.marcado != null )
									this.setState({ favoriteIcon: "ios-heart-outline" })
								else {
									this.setState({ favoriteIcon: "ios-heart" })
									this.props.navigation.state.params.place.marcado = {}
								}
							}}>
								<Icon name={ this.state.favoriteIcon} color="#ffffff" size={28} />
							</TouchableOpacity>
						</Col>
					</Row>
					<Row>
						<Col>
							<Text style={styles.placeText} >{this.props.nombre}</Text>
						</Col>
					</Row>
				</Grid>
			</ImageBackground>
		);

	}


}

function mapDispatchToProps(dispatch, ownProps){
	return {
		...ownProps,
		dispatch
	}
}

export default connect(null, mapDispatchToProps)(PlaceHeader);

let { width, height } = Dimensions.get("screen");

const styles = StyleSheet.create({

	background :{
		height:  ( height * 35 ) / 100,
		width: '100%'

	},
	gridStyle :{
		paddingTop: "8%",
		paddingLeft: '3%'
	},
	placeText :{
		color:"#ffffff",
		fontSize: 32,
		fontFamily: 'NeutraText-DemiItalic'
	}

})