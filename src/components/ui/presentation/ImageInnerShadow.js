import React, { Component } from 'react';
import LinearGrediant from 'react-native-linear-gradient';
import { View, ImageBackground, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Row, Col, Grid } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';

export default class ImageInnerShadow extends Component {

	constructor(props){
		super(props);
	}

	render(){

		return(
			<ImageBackground source={this.props.source} style={this.props.style}>

				<Row>
					<Text>Hola mundo</Text>
				</Row>
				<LinearGrediant
					start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}}
					locations={[0,0.5,0.6]}
					colors={['rgba(0,0,0, 0.5)', "rgba(0,0,0, 0.2)"]}
				>
					<Row style={{ height: "100%" }} >
						<Grid>
							<Row style={styles.innerRowH1}>
								<Text style={styles.primaryText} >
									{this.props.place}
								</Text>
							</Row>
							<Row styke={styles.innerRowH2}>
								<Col style={{width: "10%"}}>
									<Icon name="md-pin" size={24} color="#ffffff" />
								</Col>
								<Col>
									<Text style={styles.secondaryText}>{this.props.placeLocation}</Text>
									
								</Col>
							</Row>
						</Grid>
					</Row>
				</LinearGrediant>
			</ImageBackground>
		);

	}

}

const styles = StyleSheet.create({

	innerRowH1 :{
		height: '70%',
		paddingTop: "23%",
		paddingLeft: "4%" 
	},
	innerRowH2 :{
		paddingLeft: "4%"
	} ,
	primaryText :{
		color: "#FFFFFF",
		fontSize: 24,
		fontFamily: 'NeutraText-Bold',
		textAlign: 'center' 
	},
	secondaryText :{
		color: "#FFFFFF",
		fontFamily: 'NeutraText-BookItalic' 
	}

});