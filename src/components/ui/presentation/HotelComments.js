import React from 'react';
import {
    Modal,
    View,
    Text,
    TouchableOpacity,
    Alert,
    StyleSheet,
    FlatList,
    ActivityIndicator,
    ScrollView,
    Image,
    TextInput,
    KeyboardAvoidingView,
    Keyboard
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Comments from 'react-native-comments';
import moment from 'moment';
import {  SENT_COMMENT_PLACE } from '../../../redux/actions/startScreens';
import { connect } from 'react-redux';

class HotelComments extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            visible: false,
            comments: [],
            myComment: "",
            commentable: true,
            _textInpit: null
        }
    }

    componentWillReceiveProps(newProps){
       this.setState({visible: true})
       if( newProps.isNewComment ){
           let { comments } = this.state;
           comments.push( newProps.comentario );
           this.setState({  comments });
           this.props.dispatch({
               type: "COMMENT_SAVED"
           });
       }
    }

    _renderRow(item){
        return(
            <View style={{ flex: 1, flexDirection: 'column', justifyContent: "center", paddingLeft: 4, height: 100 }}>
                <View style={{ flex:1, flexDirection: "row", justifyContent: "space-around" }}>
                    <Text style={{color: "#000000", fontSize: 12}}>
                        {item.usuario.nombre}
                    </Text>
                    <Text style={{color: "#000000", fontSize: 12}}>
                        5 pts
                    </Text>
                    
                </View>
            </View>
        )
    }

    componentDidMount(){
        this.setState({
            comments: this.props.comentarios
        })
    }

     _renderComentOrLoader(){
        let { comentarios } = this.props; 
        if( Array.isArray(comentarios) && comentarios.length == 0 ){
            return (
                <View style={{ height: "100%", justifyContent: "center" }}>
                    <Text style={{textAlign: "center"}}> Este hotel aún no tiene comentarios, se el primero. </Text>
                </View>
            )
        }

        return (
            <View style={{ flex:1, flexDirection: "column", justifyContent: "center" }}>
                <View>
                    <FlatList
                        style={styles.root}
                        data={this.state.comments}
                        extraData={this.state.comments}
                        ItemSeparatorComponent={() => {
                        return (
                            <View style={styles.separator}/>
                        )
                        }}
                        keyExtractor={(item)=>{
                        return item.id;
                        }}
                        renderItem={({item}) => {
                        return(
                            <View style={styles.container}>
                            <TouchableOpacity onPress={() => {}}>
                                <Image style={styles.image} source={{uri: 'http://theivykey.com/images/no-profile-image.png'}}/>
                            </TouchableOpacity>
                            <View style={styles.content}>
                                <View style={styles.contentHeader}>
                                <Text  style={styles.name}>{ item.usuario.name }</Text>
                                <Text style={styles.time}>
                                    {moment(item.created_at).format('HH:MM')}
                                </Text>
                                </View>
                                <Text rkType='primary3 mediumLine'>{item.comentario}</Text>
                            </View>
                            </View>
                        );
                    }}/> 
                        
                </View>
            </View>       
        )
    }

    _onSentComment(comment){
        Keyboard.dismiss();
        this.props.dispatch({
            type: SENT_COMMENT_PLACE,
            data: {
                lugar_id: this.props.id,
                comentario: comment,
                rating: 5
            },
            dispatch: this.props.dispatch,
            navigation: null
        });
        this.setState({ myComment: "" });
    }
    render(){
        return(
            <Modal
            transparent={true}
            visible={this.state.visible}
            onRequestClose={()=> this.props.onRequestClose({isOpen: false}) }
            animationType="slide"
            >
                <View style={styles.mainView}>
                    <View style={styles.headerModal}>
                        <View style={{ flex: 1, flexDirection: "row", justifyContent: "space-around", height: "100%", alignItems: "center" }}>
                            <Text style={{color:"#ffffff", fontSize: 26}}>
                                Comentarios de los usuarios
                            </Text>
                            <TouchableOpacity onPress={()=>{ this.setState({visible: false}); }}>
                                <Icon name="md-close" color="#ffffff" size={26} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.bodyModal}>
                        {this._renderComentOrLoader()}
                    </View>
                    
                    <View style={{ backgroundColor: "#ffffff", bottom: 0, left: 0, right: 0, height: "11%", justifyContent: "flex-end" }}>
                        <View style={{ width: "100%", flex: 1, flexDirection: "row" }}> 
                            <View style={{ width: "90%", bottom: 0, right: 0, left: 0,flex: 1, flexDirection: "column", justifyContent: "flex-end" }}>
                                <TextInput
                                    underlineColorAndroid="#ef5350"
                                    multiline={true}
                                    placeholder="Ingrese su comentario"
                                    onChangeText={(text)=> this.setState({myComment: text}) }
                                    value={this.state.myComment}
                                />
                            </View>
                            <View style={{width: "10%", alignSelf: "center", alignItems: "center" }}>
                                <TouchableOpacity onPress={()=> this._onSentComment( this.state.myComment) } style={{bottom: 0, left: 0, right: 0}}>
                                    <Icon name="md-send" color="#ef5350" size={30} />
                                </TouchableOpacity>
                            </View> 
                        </View>
                    </View>
                    
                </View>
            </Modal>
        )
    }
}

function mapStateToProps(state){
    let comentario = state.comment.newComment;
    if( comentario != null ) comentario.usuario = state.comment.user;
    return {
        newComment: state.comment.newComment,
        user: state.comment.user,
        comentario,
        isNewComment: comentario == null ? false : true
    };
}

export default connect(mapStateToProps, null)(HotelComments);

const styles = StyleSheet.create({
    mainView:{ 
         backgroundColor: "#ffffff", 
         width: "100%", 
         flex: 1,
         flexDirection: "column"
    },
    headerModal : {
        backgroundColor: "#ef5350",
        paddingLeft: 7,
        paddingRight: 7,
        height: "10%",
    },
    bodyModal:{
        height:"75%",
        width: "100%",
        flex: 1,
        flexDirection:"column"
    },
    root: {
        backgroundColor: "#ffffff",
        marginTop:10,
        top: 0,
        left: 0,
        right: 0
      },
      container: {
        paddingLeft: 19,
        paddingRight: 16,
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'flex-start'
      },
      content: {
        marginLeft: 16,
        flex: 1,
      },
      contentHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 6
      },
      separator: {
        height: 1,
        backgroundColor: "#CCCCCC"
      },
      image:{
        width:45,
        height:45,
        borderRadius:20,
        marginLeft:20
      },
      time:{
        fontSize:11,
        color:"#808080",
      },
      name:{
        fontSize:16,
        fontWeight:"bold",
      },
})