import React, { Component } from 'react';

import { View, StyleSheet, TouchableOpacity, Alert, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import IconBadge from 'react-native-icon-badge';


class ShopingCartHeaderIcon extends Component{

	constructor(props){
		super(props);
	}


	render(){

		return(
			<IconBadge
				MainElement={
					<TouchableOpacity style={styles.touchable} onPress={()=>{ this.props.navigation.navigate("cart") }} >
						<View>
							<Icon name="md-cart" size={30} color="#ffffff" />
						</View>
					</TouchableOpacity>
				}

				BadgeElement={
					<Text style={styles.textIconBadge}>1</Text> 
				}

				IconBadgeStyle={
				   {
				   		width:14,
				    	height:20,
				    	backgroundColor: "#CD000A",
				    	marginTop: -5,
				    	marginLeft: -9
				    }
				}

				Hidden={false}

			/>
		)
	}

}


function mapDispatchToProps(dispatch, ownProps){

	return {
		...ownProps
	}
}

export default connect( null, mapDispatchToProps )(ShopingCartHeaderIcon);

const styles = StyleSheet.create({

	touchable: {
		marginRight: 14
	},
	badgeStyles:{
		backgroundColor: '#CD000A',
		marginBottom: -3,
		height: 22
	},
	textIconBadge:{
		color: "#ffffff"
	}

})