import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import LinearGradiant from 'react-native-linear-gradient';


export default (props)=>{
    return(
        <ImageBackground source={ props.image } style={{ width: 175, height: 190, marginLeft: 2.5 }}>
            
            <View style={{ flex: 1, flexDirection: 'column', justifyContent: "flex-end" }}>
                <LinearGradiant   
                    start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}}
                    locations={[0,0.5,0.6]}
                    colors={['rgba(0,0,0,0.8)', 'rgba(0,0,0,0.4)', 'rgba(0,0,0,0.2)']} 
                    style={{ paddingLeft: 5 }}
                >
                <Text style={[styles.text, styles.textName]}>
                    {props.name}
                </Text>
                <Text style={[styles.text, styles.textLocation]}>
                    <Icon name="md-pin" size={18} color="#ffffff" />
                    {' '+props.location}
                </Text>

                 </LinearGradiant>
            </View>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    text: {
        color: "#ffffff",
        fontFamily: "Roboto",
        fontWeight: 'bold'
    },
    textName: {
        fontSize: 22
    },
    textLocation: {
        fontSize: 18
    }
})