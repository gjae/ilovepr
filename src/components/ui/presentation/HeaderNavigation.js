import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, ImageBackground, TouchableOpacity, Alert } from 'react-native';
import { Row, Col } from 'react-native-easy-grid'
import Picker from 'react-native-picker-select';
import Icon from 'react-native-vector-icons/Ionicons';
import i18n from '../../../config/translation/i18n';


class HeaderNavigation extends Component {

	constructor(props){
		super(props);
		this.state = {
			ciudades: [],
			ciudad: 0
		}
	}

	componentWillMount(){
		i18n.locate = this.props.lang;
	}
	componentDidMount(){
		this.props.dispatch({
			type: "GET_CIUDADES"
		});
	}

	componentWillReceiveProps(newProps){
		let { ciudades } = this.state;
		ciudades = ciudades.concat(newProps.ciudades);
		this.setState({ciudades});
	}

	render(){

		return(
			<ImageBackground source={require("../assets/img/login_background.jpg")} style={styles.imageBackgroundStyle} >
				<Row>
					<Col style={{ width: "60%", height: "100%", flex: 1, flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
						<Picker                     
							placeholder={{
								label: 'Todas las ciudades...',
								value: "todas",
							}} 
							items={this.state.ciudades}  
							onValueChange={(item) => {
								this.setState({ciudad: item});
								this.props.dispatch({type: "SET_CIUDAD", ciudad: item});
							}}
							itemTextColor="#ffffff" 
							style={{borderBottomColor: "#ffffff", borderBottomWidth: 1, color:"#ffffff"}} 
							textInputProps={{
								style: {
									color: "#ffffff"
								},
								underlineColorAndroid: "#ffffff"
							}}
							pickerProps={{
								color:"#ffffff" , 
								itemStyle: {color: "#ffffff"}
							}}
							placeholderTextColor="#ffffff"
							style={{
								underline:{
									borderBottomColor: "#ffffff"
								},
								headlessAndroidContainer:{
									width: "60%"
								}
							}}
							
						/>
					</Col>
				</Row>
				<Row style={styles.rowNavigation}>
					<Col>
						<TouchableOpacity style={styles.touchableArea} onPress={()=>{ this.props.navigation.navigate("places", {tipo: "Lugares", buscar: "marcados"}) }}>
							<Icon name="ios-heart-outline" size={40} color="#000000" />
							<Text style={styles.navigationText}>
								{ i18n.t("favorite") }
							</Text>
						</TouchableOpacity>
					</Col>
					<Col>
						<TouchableOpacity style={styles.touchableArea} onPress={ ()=> this.props.navigation.navigate("events", {buscar: "marcados", title: "Eventos Marcados"}) }>
							<Icon name="ios-calendar-outline" size={40} color="#000000" />
							<Text style={styles.navigationText}>
								{ i18n.t("ev-marked") }
							</Text>
						</TouchableOpacity>
					</Col>
					<Col>
						<TouchableOpacity style={styles.touchableArea} onPress={()=> this.props.navigation.navigate('map', {buscar: "todo"})}>
							<Icon name="ios-map-outline" size={40} color="#000000" />
							<Text style={styles.navigationText}>
								{ i18n.t("map") }
							</Text>
						</TouchableOpacity>
					</Col>
				</Row>
			</ImageBackground>
		);

	}

}


function mapDispatchToProps(dispatch){
	return { dispatch }
}

function mapStateToProps(state){
	return {
		lang: state.lang.lang,
		ciudades:state.places.ciudades
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderNavigation);

const styles = StyleSheet.create({
	imageBackgroundStyle: {

		width: "100%",
		height: 220

	},
	rowNavigation :{
		backgroundColor: '#ffffff',
		opacity: 0.7,
		height: "25%",
		paddingTop: 3,
		paddingBottom: 4,
		paddingLeft: 9,
		paddingRight: 9
	},
	navigationText :{
		color: "#000000",
		fontSize: 15,
		marginTop: "-3.4%"
	},
	touchableArea :{
		alignSelf: "center",
		alignItems: "center"
	}
});