import React from 'react';
import {
    View,
    StyleSheet,
    StatusBar,
    Text,
    FlatList,
    TouchableOpacity,
    ActivityIndicator,
    Alert
} from 'react-native';
import { SafeAreaView } from 'react-navigation';

import { connect } from 'react-redux';
import Card from '../presentation/Card';
import CardInfo from '../presentation/CardInfo';
import Icon from 'react-native-vector-icons/Ionicons';
import {
    GET_SERVER_HOTELS
} from '../../../redux/actions/startScreens';
import env from '../../../config/env';

class Places extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            hotels: [],
            load: this.props.load
        }
    }

    _renderStars = (count) => {
        if( true ){
            let stars = [];
            for(var i = 0; i< count; i++){
                stars.push(
                    <Icon name="md-star" color="#000000" size={18} />
                )
            }
            return stars;
        }

        return null;
    }

    componentWillReceiveProps(newProps){
        if( newProps.hotels.length > 0 )
            this.setState({ hotels: newProps.hotels });
    }

    _imagesArray = (item) => {
        let imagenes = [];
        for(let i = 0; i < item.length; i++){
            imagenes.push( env.base_url+'/'+item[i].imagen.ubicacion );
        }
        return imagenes;

    }

    _renderRow = ({item}) => {
        return (
            <CardInfo images={this._imagesArray(item.imagenes)}>
                <View style={{ flex: 1, flexDirection: "column", paddingTop: 3, paddingLeft: 3, paddingRight: 3 }}>
                  
                    <View style={{ flex: 1, flexDirection: "row", justifyContent: 'space-between', height: "15%", zIndex: 1000 }}>
                        <Text style={{ fontSize: 18, fontWeight: "bold", color: "#000000", textAlign: "left" }} >{item.nombre}</Text>                        
                        <Text style={{ fontSize: 18, fontWeight: "bold", color: "#000000", textAlign: "right" }}>{item.hotel.costo_dia+' $'} </Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row", justifyContent: 'space-between'}}>
                        <Text style={{ fontSize: 18, color: "#000000", textAlign: "right" }}>
                            <Icon name="ios-pin-outline" color="#000000" size={18} /> {item.direccion}
                        </Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row", justifyContent: 'space-between'}}>
                        <Text style={{ fontSize: 18, fontWeight: "bold", color: "#000000", textAlign: "right" }}> 
                            {this._renderStars(item.stars)} { item.hotel.rating >= 3 ? 'Muy bueno': '' } 
                        </Text>
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate('hotelDetails', {hotel: item, imagenes: this._imagesArray(item.imagenes), comentarios: item.comentarios})} style={{ width: 100, height: 20, alignItems: "center", justifyContent: "center", backgroundColor: "#4E9A06" }} >
                            <Text style={{ color:"#ffffff", fontSize: 14, fontWeight: "bold" }}>
                                Ver
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </CardInfo>
        )
    }

    componentDidMount(){
        this.props.dispatch({
            type: GET_SERVER_HOTELS,
            dispatch: this.props.dispatch,
            navigation: this.props.navigation
        });
    }


	_renderContent(){
		let { hotels } = this.state;
		if(  this.props.load ){
			return(
				<View style={{ flex: 1, flexDirection: "column", justifyContent: "center" }}>
					<ActivityIndicator color={"#ef5350"}/>		
				</View>
			)
		}
		if( this.props.hotels.length == 0  && !this.props.load ){
			return(
				<View style={{ flex: 1, flexDirection: "column", alignItems: "center", justifyContent: "center", height: "100%", width: "100%" }}>
					<Icon name="ios-happy-outline" size={250} color="#000" />
					<Text style={{ textAlign: "center" }}>
						Lo sentimos, en este momento no hay información en esta categoría, trabajamos para incluir la 
						información que te damos a conocer
					</Text>	
				</View>)
		}

		else{
			return <FlatList
                data={this.props.hotels}
                keyExtractor={ (item, index) => item.id }
                renderItem={ (item)=> this._renderRow(item) }
                numColumns={1}
                style={{marginTop: 5}}
            />

		}
	}

    render(){
        return(
            <SafeAreaView style={styles.mainView}>  
                <StatusBar
                    translucent={false}
                    backgroundColor={"#ef5350"}
                />
                {this._renderContent()}
            </SafeAreaView>

        )
    }
}

Places.navigationOptions = (props)=> ({
    title: props.navigation.state.params.sideList
});

const styles = StyleSheet.create({
    mainView: {
        height: "100%",
        width: "100%",
        backgroundColor: "#eeeeee"
    }
})

function mapStateToProps(state, ownProps){
    return {
        hotels: state.hotels.hotels,
        load: state.hotels.load
    }
}

export default connect(mapStateToProps, null)(Places);