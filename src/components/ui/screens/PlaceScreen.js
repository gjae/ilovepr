import React, { Component } from  'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { connect } from 'react-redux';
import Header from '../presentation/PlaceHeader';
import Tabs from '../../../config/navigation/TabPlaceNavigation';
import env from '../../../config/env';

class PlaceScreen extends Component {

	static navigationOptions = ({navigation}) => ({
		header: ()=> <Header  
			nombre={navigation.state.params.place.nombre} 
			image={{ uri: env.base_url+'/'+navigation.state.params.place.imagenes[0].imagen.ubicacion  }} 
			navigation={navigation} 
			place={navigation.state.params.place}
			/>
	})

	constructor(props){
		super(props);
	}

	render(){

		return(
			<SafeAreaView style={styles.mainView} >
				<StatusBar
					translucent={true}
					backgroundColor='transparent'
				/>
				<Tabs screenProps={{ place: this.props.navigation.state.params.place, navigation: this.props.navigation }} />
			</SafeAreaView>
		);

	}
}

export default connect(null, null)(PlaceScreen);

const styles = StyleSheet.create({

	mainView :{
		backgroundColor: '#ffffff',
		width: '100%',
		height: '100%'
	}

})