import React, {Component} from 'react';
import {
	Col,
	Row,
	Grid
} from 'react-native-easy-grid';

import {
	View,
	StatusBar,
	StyleSheet,
	Text,
	TouchableOpacity,
	Alert,
	Image
} from  'react-native';

import { Dropdown } from 'react-native-material-dropdown';
import { connect } from 'react-redux';
import { SAVE_NEW_SETTINGS, PERSIST_LANG } from '../../../redux/actions/startScreens';
import i18n from '../../../config/translation/i18n';

class InitialAppConfig extends Component{

	constructor(props){
		super( props );
		this.state = {
			lang: this.props.lang
		}

		this._onChangeText = this._onChangeText.bind(this);
		this._onAcceptPress = this._onAcceptPress.bind(this);
	}

	componentWillMount(){
		i18n.locale = this.props.lang;
	}

	_onChangeText = ( value, index, data ) => {
		let newLang = 'es';
		switch( value ){
			case 'Español / Spanish': 
				newLang = 'es';
			break;
			case 'Ingles / English' :
				newLang = 'en';
			break;
		} 

		i18n.locale = newLang;
		this.setState({
			lang: newLang
		})
		this.forceUpdate();
	}

	_onAcceptPress = () =>{
		this.props.dispatch({ 
			type: PERSIST_LANG, 
			content: { lang: this.state.lang } ,
			navigation: this.props.navigation
		});
	}

	render(){

		return(

			<View style={styles.mainView}>
				<StatusBar
					translucent={false}
					backgroundColor={"#ef5350"}
				/>

				<Grid style={styles.gridContainer}>
					<Row >
						<Dropdown
							laben={"Lenguaje / Language"}
							data={[
								{value: "Español / Spanish"},
								{value: "Ingles / English" }
							]}
							containerStyle={styles.dropdownContainer}
							itemTextStyle={styles.dropdownText}
							pickerStyle={styles.dropdownPicker}
							itemPadding={8}
							rippleCentered={true}
							dropdownOffset={{top: 12, left: 3}}
							fontSize={24}
							useNativeDriver={true}
							onChangeText={ this._onChangeText }
						/>
					</Row>
					<Row>
						<Text style={styles.welcomeText} >
						{ i18n.t('languageSettings') }
						</Text>
					</Row>
					<Row>
						<TouchableOpacity style={styles.touchableOpacityStyle} onPress={ this._onAcceptPress }>
							<Text style={styles.textButton}>
								{ i18n.t('acceptBtnText') }
							</Text>
						</TouchableOpacity>
					</Row>
				</Grid>

			</View>
		)
	}

}


function mapStateToProps(state){
	return {
		lang: state.lang.lang
	}
}

function mapDispatchToProps(dispatch, ownProps){
	return {
		dispatch,
		navigation: ownProps.navigation
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(InitialAppConfig);

const styles = StyleSheet.create({
	touchableOpacityStyle: {
		borderWidth: 1,
		borderColor: '#ef5350',
		width: '95%',
		height: 52,
		borderRadius: 4,
		marginLeft: 5,
		alignContent: 'center',
		alignItems: 'center',
		paddingTop: 4
	},
	textButton: {
		color: "#888A85",
		fontSize: 32,
		textAlign: 'center' 
	},
	mainView: {
		backgroundColor: '#FFFFFF',
		height: '100%',
		width: '100%',
		paddingTop: '50%'
	},
	welcomeText: {
		color: "#888A85",
		textAlign: 'center' 
	},
	gridContainer:{
		paddingLeft: 5,
		paddingRight: 5
	},
	dropdownContainer: {
		width: '95%',
		marginLeft: 5,
		height: 57,
		borderWidth: 1,
		borderRadius: 3,
		borderColor: '#ef5350',
		paddingLeft: 7,
		paddingRight: 7
	},
	dropdownText:{
		fontSize: 120,
		textAlign: 'center' 
	},
	dropdownPicker: {
		alignContent: 'center',
		alignSelf: 'center'  ,
		alignItems: 'center' 
	}
})