import React from 'react';
import {
    View,
    Image,
    Text,
    StyleSheet,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-navigation';
import ImageSlider from 'react-native-image-slider';
import { 
    Avatar
 } from 'react-native-elements';
 import Divider from '../presentation/Divider'
 import Icon from 'react-native-vector-icons/Ionicons';

class PlaceDetails extends React.Component{

    constructor(props){
        super(props);
    }

    _renderStars = () => {
        if( this.props.navigation.state.params.is == 'hotels' ){
            let stars = [];
            for(var i = 0; i< this.props.navigation.state.params.place.stars; i++){
                stars.push(
                    <Icon name="md-star" color="#000000" size={28} />
                )
            }
            return stars;
        }

        return null;
    }

    render(){
        let { navigation } = this.props;
        return(
            <SafeAreaView style={styles.safeArea}>
                <View style={{ flex: 1, flexDirection: "column", justifyContent: "center" }}>
                    <View style={{ flex: 1, flexDirection: "row", justifyContent: "center" }} >
                        <ImageSlider
                            loopBothSides
                            autoPlayWithInterval={3000}
                            images={navigation.state.params.place.images}
                        />    
                    </View>
                    <View style={{ flex: 1, flexDirection: "row", justifyContent: "center" }} >
                        <View style={{ backgroundColor: "#ef5350", height: 90, width: "50%", paddingLeft: 4, flex: 1, flexDirection: 'column' }}>
                            <Text style={{ color:"#ffffff", fontWeight: "bold" }} >Costo por noche: $1000</Text>
                            <Text style={{ color:"#ffffff", fontWeight: "bold" }}>Horario:  06:00 - 20:00</Text>
                        </View>
                        <View style={{ backgroundColor: "#ffffff", height: 90, width: "50%",  justifyContent: "center" }}>
                            <Text style={{ color:"#000000", fontWeight: "bold", fontSize: 22, textAlign: "center" }} >Estrellas </Text>
                            <View style={{ flex: 1, flexDirection: "row",  justifyContent: "center" }}>
                                {this._renderStars()}
                            </View>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        )
    }

}

PlaceDetails.navigationOptions = ({navigation}) =>({
    title: navigation.state.params.place.name
});

const styles = StyleSheet.create({
    safeArea: {
        backgroundColor: "#ffffff",
        width: "100%",
        height: "100%",
        paddingTop: 0.9
    },
    topView:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    }
})

export default connect(null, null)(PlaceDetails);
