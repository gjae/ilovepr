import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, FlatList, TouchableOpacity, Image, ImageBackground, ScrollView, Linking, Alert, Platform } from 'react-native';
import { Row, Col, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import { Divider } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import Header from '../presentation/EventHeader'
import env from '../../../config/env';
import {
	MARK_PLACE
} from '../../../redux/actions/startScreens';


import Share, {ShareSheet, Button} from 'react-native-share';
import ShareIcons from '../../../config/social-share'

class EventDetails extends Component {

	static navigationOptions = ({ navigation }) => ({
		header: ()=> <Header 
			navigation={navigation} 
			source={{ uri: env.base_url+'/'+navigation.state.params.event.imagenes[0].imagen.ubicacion }} 
			event={navigation.state.params.event}
		/>

	})

	constructor(props){
		super(props);

		let navParams = this.props.navigation.state.params.event;
		this.state = {
			...navParams,
			iconMarcado: navParams.marcado == null || navParams.marcado.marcado == null ? "ios-star-outline" :'ios-star',
			iconAsistencia: navParams.marcado == null || navParams.marcado.quiero_asistir == null ? "ios-checkmark-circle-outline" : "ios-checkmark-circle"
		};
		this._onMarkPress = this._onMarkPress.bind(this);
	}

	_onMarkPress = (action) =>{
		this.props.dispatch({
			type: MARK_PLACE,
			data: action
		});
	}

	_meInteresa = () =>{
		let data ={
			is: "interes",
			to: this.state.marcado == null || this.state.marcado.marcado == null ? 'mark' :  "dismark",
			evento_id: this.state.id,
			resource: "events/save",
			lugar_id: 1
		};

		this._onMarkPress(data);
		if( data.to == "mark" ){
			this.setState({ iconMarcado: "ios-star" });
			this.setState({ marcado: { marcado: {} } });
		}else{
			this.setState({ iconMarcado: "ios-star-outline" });
			this.setState({ marcado: null });

		}
	}
	_asistencia(){
		let data ={
			is: "asistencia",
			to: this.state.marcado == null || this.state.marcado.quiero_asistir == null ? 'mark' :  "dismark",
			evento_id: this.state.id,
			resource: "events/save",
			lugar_id: 1
		};

		this._onMarkPress(data);
		if( data.to == "mark" ){
			this.setState({ iconAsistencia: "ios-checkmark-circle" });
			this.setState({ marcado: { quiero_asistir: {} } });
		}else{
			this.setState({ iconAsistencia: "ios-checkmark-circle-outline" });
			this.setState({ marcado: null });

		}
	}

	render(){
		return(
			<Grid style={styles.mainView}>
				<StatusBar translucent={true} backgroundColor="transparent" />

				<Row style={styles.optionsRow}>
					<Col>
						<TouchableOpacity style={styles.options} onPress={()=> this._meInteresa() }>
							<Icon name={this.state.iconMarcado}  color="#ffffff" size={32} />
							<Text style={[styles.optionText, {color: '#ffffff'}]}>Me interesa</Text>
						</TouchableOpacity>
					</Col>
					<Col>
						<TouchableOpacity style={styles.options} onPress={()=> this._asistencia()}>
							<Icon name={this.state.iconAsistencia}  color="#ffffff" size={32} />
							<Text style={[styles.optionText, {color: '#ffffff'}]}>Quiero asistir</Text>
						</TouchableOpacity>
					</Col>
					<Col>
						<TouchableOpacity style={styles.options} onPress={()=>{
							const shareOptions = {
								title: this.state.nombre,
								message: 'He compartido esto desde ILovePR',
								subject: this.state.nombre,
								url: env.base_url+'/'+this.props.navigation.state.params.event.imagenes[0].imagen.ubicacion,
							};
							Share.open(shareOptions);
						}}>
							<Icon name="ios-share-alt"  color="#ffffff" size={32} />
							<Text style={[styles.optionText, {color: '#ffffff'}]}>Compartir</Text>
						</TouchableOpacity>

					</Col>
					<Col>
						<TouchableOpacity style={styles.options} onPress={()=>{
							Linking.openURL(`google.navigation:q=${this.state.latitud}+${this.state.longitud}`)							
						}}>
							<Icon name="ios-map-outline"  color="#ffffff" size={32} />
							<Text style={[styles.optionText, {color: '#ffffff'}]}>Ver en el mapa</Text>
						</TouchableOpacity>

					</Col>
				</Row>
				<Row style={{ height: '24.5%', borderBottomWidth: 1, borderBottomColor: '#BABDB6' }}>
					<Col style={{ width: '50%', borderRightColor: '#BABDB6', borderRightWidth: 1, paddingLeft: 3 }}>
						<Row style={{ alignSelf: "center", alignItems: "center" }}>
							<Col style={{ width: '15%' }}>
								<Icon name="md-pin" color="#ef5350" size={32} />
							</Col>
							<Col>
								<Text style={styles.textInfoPrimary}>Lugar del evento</Text>
							</Col>
						</Row>
						<Row>
							<Col>
								<Text style={[styles.textInfoPrimary, {fontSize: 13}]}>{this.state.direccion}</Text>
							</Col>
						</Row>
					</Col>
					<Col style={{ width: '50%', paddingLeft: 3 }}>
						<Row style={{ alignSelf: "center", alignItems: "center" }}>
							<Col style={{ width: '15%' }}>
								<Icon name="md-calendar" color="#ef5350" size={32} />
							</Col>
							<Col>
								<Text style={styles.textInfoPrimary}>Fecha del evento</Text>
							</Col>
						</Row>
						<Row>
							<Col>
								<Text style={[styles.textInfoPrimary, {fontSize: 13}]}>{this.state.fecha_hora}</Text>
							</Col>
						</Row>
						<Row>
							<Col>
								<Text style={[styles.textInfoPrimary, {fontSize: 13}]}>Tipo de evento: {this.state.tipo}</Text>
							</Col>
						</Row>
					</Col>
				</Row>
				<ScrollView>
					<Row style={{ paddingLeft: 7, paddingTop: 5 }}>
						<Col>
							<Text style={styles.textContactInfo}>
								<Icon name="md-call" color="#000000" size={30} /> {"  "+this.state.organizador.phone }
								{"\n\n"}
								<Icon name="md-mail" color="#000000" size={30} /> {"  "+this.state.organizador.email}
								{"\n\n"}
								<Icon name="logo-wordpress" color="#000000" size={30} /> {"  "+this.state.organizador.website}
								{"\n\n"}
								<Icon name="md-mail" color="#000000" size={30} /> {"  "+this.state.organizador.email}
								{"\n\n"}
								<Icon name="md-pricetag" color="#000000" size={30} /> {"  "+this.state.costo_entrada}
							</Text>
						</Col>
					</Row>
				</ScrollView>
			</Grid>
		);
	}

}

export default connect(null, null)(EventDetails);

const styles= StyleSheet.create({

	mainView :{
		height: '100%',
		width: '100%',
		backgroundColor: '#FFFFFF'
	},
	optionsRow :{
		paddingLeft: 3,
		paddingRight: 3,
		paddingTop: 6.5,
		paddingBottom: 3,
		borderBottomWidth: 1,
		borderBottomColor: '#BABDB6',
		height: '17%',
		backgroundColor: '#ef5350',
		elevation: 0.5
	},
	options :{
		alignSelf : "center",
		alignItems: "center"
	},
	optionText :{
		fontFamily: 'BebasNeue',
		fontSize: 16
	},
	textInfoPrimary :{
		color: "#000000",
		fontSize: 18,
		fontFamily: 'BebasNeue'
	},
	textContactInfo :{
		color: "#000000",
		fontFamily: 'NeutraText-Bold',
		fontSize: 20
	}

})