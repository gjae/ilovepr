import React, {Component} from 'react';
import {
	Col,
	Row,
	Grid
} from 'react-native-easy-grid';

import {
	View,
	StatusBar,
	StyleSheet,
	Text,
	TouchableOpacity,
	Alert,
	Image,
	PermissionsAndroid,
	Platform
} from  'react-native';

import { Dropdown } from 'react-native-material-dropdown';
import { connect } from 'react-redux';
import FadeIn from 'react-native-fade-in-image';
import Settings from '../../../config/storage';
import { VERIFY_SETTINGS } from '../../../redux/actions/startScreens';

class SplashScreen extends Component{

	constructor(props){
		super(props);
	}

	componentWillMount(){
		this.props.dispatch();
	}

	render(){
		return(
			<View style={styles.containerView} >
				<StatusBar
					translucent={false}
					backgroundColor={"#ef5350"}
				/>
				<FadeIn>
					<Image source={require('../../../assets/img/logo.png')} style={ styles.imgContainer } />
				</FadeIn>
			</View>
		)
	}

}

function mapDispatchToProps(dispatch, ownProps){

	return {
		dispatch: () =>{
			dispatch( {type: VERIFY_SETTINGS, action: { lang: "es" }, navigation: ownProps.navigation, dispatch: dispatch } );
		},
		navigation: ownProps.navigation
	}

}

function mapStateToProps(state){
	return {
		lang: "es"
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);

const styles = StyleSheet.create({

	containerView: {
		backgroundColor: '#FFFFFF',
		width: '100%',
		height: '100%',
		paddingTop: '45%',
		paddingLeft: "15%"
	},
	imgContainer: {
		width: 250,
		height: 250,
		borderRadius: 300
	}

})