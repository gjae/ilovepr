import React from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TouchableHighlight,
    StatusBar,
    Geolocation,
    PermissionsAndroid,
    Platform,
    Alert,
    ActivityIndicator,
    Dimensions,
    TouchableOpacity,
    FlatList,
    Linking
} from 'react-native'
import { connect } from 'react-redux';
import MapView , { PROVIDER_GOOGLE, Marker, Callout }from 'react-native-maps';
import { SafeAreaView } from 'react-navigation';
import Interactable from 'react-native-interactable';
import env from '../../../config/env';
import Icon from 'react-native-vector-icons/Ionicons';
import {
    GET_SERVER_PLACES
} from '../../../redux/actions/startScreens';
import { Tile } from 'react-native-elements';

const ScreenDimensions ={
    height: Dimensions.get('screen').height,
    width: Dimensions.get('screen').width
}

const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      height: "100%",
      width: 400,
      justifyContent: 'center',
      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
      height: "100%"
    },
    panelContailer: {
        ...StyleSheet.absoluteFillObject
    },
    panel: {
      height: ScreenDimensions.height + 300,
      backgroundColor: "#fff",
      elevation: 2.6
    },
});
class MapScreen extends React.Component{

    static navigationOptions = ({ navigation }) =>({
        header: () => null,
        headerStyle: {
            backgroundColor: "#ffffff",
            elevation: 0
        },
        title: "Mapa"
    })

    constructor(props){
        super(props);
        this.state = {
            position: {
                longitude: null,
                latitude: null,
                longitudeDelta: null,
                latitudeDelta: null,
                accuracy: null
            },
            markers: [],
            loadInitialRegion: true,
            currentPosition: "Cargando localización...",
            places: [],
            placesByFilter: [],
            loadPlaces: this.props.load,
            dragableCard: true,
            region: null,
            direccion: "",
            viewButtom: false
        }
        this.markers = [];
    }

    async setPosition(){
        navigator.geolocation.getCurrentPosition( 
            position =>{
                let iposition = this.regionFrom(position.coords.latitude,position.coords.longitude, position.coords.accuracy);
                this.props.dispatch({
                    type: GET_SERVER_PLACES+"_ALL",
                    data: {
                        tipo :"todo",
                        longitud: position.coords.longitude,
                        latitud: position.coords.latitude
                    }
                });
                this.setState({
                    position: iposition ,
                    loadInitialRegion: false,
                    region: iposition
                });
                fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + position.coords.latitude + ',' +  position.coords.longitude + '&key=' + env.google_key)
                .then( resp => resp.json())
                .then(resp =>{
                   // Alert.alert("--", JSON.stringify(resp))
                    this.setState({
                        currentPosition: resp.results[0].formatted_address
                    });
                });
            },
            error=>{
                console.log("--",JSON.stringify(error))
            },{enableHighAccuracy: true, timeout: 30000}
        )
    }

    regionFrom(lat, lon, accuracy) {
        const oneDegreeOfLongitudeInMeters = 111.32 * 1000;
        const circumference = (40075 / 360) * 1000;
    
        const latDelta = accuracy * (1 / (Math.cos(lat) * circumference));
        const lonDelta = (accuracy / oneDegreeOfLongitudeInMeters);
    
        return {
          latitude: lat,
          longitude: lon,
          latitudeDelta: Math.max(0, latDelta),
          longitudeDelta: Math.max(0, lonDelta)
        };
    }
    

    async componentDidMount(){

        try{
            if(Platform.OS == "android"){
                let permission = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{
                    title: "Geolocation permission",
                    message: "This app need geolocation access"
                });
                if( permission !== PermissionsAndroid.RESULTS.GRANTED ){
                    this.props.navigation.goBack();
                }

            }
        }catch(e){

        }
        this.setPosition();

    }

   componentWillReceiveProps(newProps){
       if( !newProps.load ){
           let { places } = this.state;
           places = places.concat(newProps.places);
           if( places.length > 0 ){
               this.setState({ places })
           }
       }

   }

    _renderMapOrLoader(){
        let { position } = this.state;
        
        if( this.state.loadInitialRegion ){
            return(
                <View style={{ width: "100%", height: "100%", flex:1, flexDirection: "column", justifyContent: "center" }}>
                    <ActivityIndicator color="#ef5350" />
                </View>
            )
        }else if( !this.state.loadInitialRegion &&  position.latitude == null ){
            return(
                <View style={{ width: "100%", height: "100%", flex:1, flexDirection: "column", justifyContent: "center" }}>
                    <Text style={{ textAlign: "center", fontSize: 22 }}>
                        Algo ha ocurrido al intentar cargar su ubicación actual, verifique que tiene el GPS activado.
                    </Text>
                </View>
            )

        }else{
            return(
                <MapView
                    provider={ PROVIDER_GOOGLE }
                    style={styles.map}
                    initialRegion={this.state.position}
                    region={this.state.region}
                >
                    <Marker
                        title={"Yo"}
                        coordinate={this.state.position}
                    />
                    {this._renderMarkers()}
                </MapView>
            )
        }
    }
    
    _renderMarkers(){
        if( this.state.places.length <= 0 ) return null;
        else{
            const markers = this.state.places.map( (item)=>{
                return <Marker
                            title={item.nombre}
                            coordinate={{ longitude: parseFloat(item.longitud) , latitude: parseFloat(item.latitud) }} 
                            key={item.id}
                            ref={ (ref) => { this.markers[ "marker-"+item.id ] = ref; } }
                        >
                            <Callout>
                                    <Text style={{textAlign: "center", color: "#000"}}>
                                        {item.nombre}
                                    </Text>
                            </Callout>
                        </Marker>
            } )

            return markers;
        } 
    }

    _openMapNavigation(){
        let{ region, position } = this.state;
        let openUrl = Platform.select({
            android: `google.navigation:q=${region.latitude},${region.longitude}`
        })
        //lat: 10.62219745 lon: -71.6609387
        //Alert.alert(openUrl)
        Linking.openURL(openUrl)
    }

    _callCallout(item){
        this.markers["marker-"+item.id].showCallout();
        this.setState({
            region: {
                longitude: parseFloat(item.longitud),
                latitude: parseFloat(item.latitud),
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
            currentPosition: item.nombre+" ("+ (Math.round(item.distancia * 100) / 100 )+" KM/Aprox)",
            direccion: item.direccion,
            viewButtom: true
        })
    }

    _renderFlatList(){
        if( this.state.places.length > 0 ){
            return(
                <FlatList
                    data={this.state.places}
                    renderItem={ ({item})=> {
                        return(
                            <View style={{ width: "90%" }}>
                                <Tile
                                    imageSrc={{uri: env.base_url+'/'+item.imagenes[0].imagen.ubicacion }}
                                    title={item.nombre}
                                    caption={`${item.tipo.split("_").join(" ")}`}
                                    featured
                                    onPress={()=>{ this._callCallout(item) }}
                                />
                            </View>
                        )
                    }}
                    keyExtractor={ (item, id) => item.id}
                />
            )
        }

        return null;
    }

    _viewButtom(){
        if(this.state.viewButtom){
            return(

                <TouchableOpacity style={{ 
                        position: "absolute", 
                        height: 70,
                        width: 70, 
                        backgroundColor: "#fff", 
                        elevation: 3, 
                        borderRadius: 50, 
                        right: 47,
                        top: 0,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    onPress={()=> this._openMapNavigation()}
                >
                    <Text>
                        <Icon name={"md-navigate"} color={"#ef5350"} size={44} />    
                     </Text>
                    
                </TouchableOpacity>
            )
        }
        else return null;
    }

    render(){
        return(
            <View style={styles.container}>
                <StatusBar translucent={false} backgroundColor="#ef5350" />

                {this._renderMapOrLoader()}
                <View style={{ 
                            width: "80%", 
                            height: 50,
                            borderRadius: 7, 
                            backgroundColor: "#ffffff", 
                            elevation: 2.5, 
                            position: "absolute" ,
                            top: 10,
                            left: (ScreenDimensions.width * 5) / 100,
                            right: 0,
                            flex: 1,
                            flexDirection: "row",
                            padding: 10
                        }}
                    >
                        <TouchableOpacity onPress={()=> this.props.navigation.goBack()}>
                            <Icon name="md-arrow-back" size={32} color="#000000" />
                        </TouchableOpacity>
                    </View>
                <View style={[styles.panelContailer]}>
                            
                        
                    <Interactable.View
                        verticalOnly={true}
                        snapPoints={[ 
                            { y: ScreenDimensions.height - 500, id: "end" },
                            { y: ScreenDimensions.height - 140 } 
                        ]}
                        initialPosition={{y: ScreenDimensions.height - 140 }}
                        style={{backgroundColor: "#ffffff"}}
                        boundaries={{ top: -300 }}
                        dragEnabled={this.state.dragableCard}
                        boundaries={{top: ScreenDimensions.height - 500, bottom: ScreenDimensions.height - 140 }}
                    >
                        <View style={[styles.panel]}>
                            <View style={{ 
                                padding: 10, 
                                backgroundColor: "#ef5350", 
                                height: (ScreenDimensions.height * 12) / 100
                             }}>
                                <View style={{width: "72%"}}>
                                    <Text style={{color: "#ffffff", fontSize: 18, textAlign: "left", fontWeight: 'bold'}}>
                                        {this.state.currentPosition}
                                    </Text>
                                    <Text style={{color: "#ffffff", fontSize: 18, textAlign: "left"}}>
                                        {this.state.direccion != "" ? <Icon name="md-pin" color="#ffffff" size={20}/> : null}
                                        {' '}{this.state.direccion}
                                    </Text>
                                </View>
                            </View>
                            <View style={{ height: (ScreenDimensions.height * 60) / 100 }}>
                                {this._renderFlatList()}
                            </View>
                        </View>
                        {this._viewButtom()}
                    </Interactable.View>
                    
                </View>
            </View>
        )
    }


}
function mapDispatchToProps(dispatch){
    return {
        dispatch
    }
}

function mapStateToProps(state){
    return{
        places: state.places.places,
        load: state.places.load
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(MapScreen);
