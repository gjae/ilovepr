import React, { Component } from 'react';
import { SafeAreaView } from 'react-navigation';
import { View, Text, StatusBar, StyleSheet } from 'react-native';
import {Col , Row, Grid} from 'react-native-easy-grid';


const MenuDiario = () => (
	<SafeAreaView style={styles.safeAreaViewContent}>

		<StatusBar
			translucent={false}
			backgroundColor={"#FFCB03"}
		/>
		<View>
			<Text>Hola mundo</Text>
		</View>

	</SafeAreaView>
)

export default MenuDiario;

const styles = StyleSheet.create({

	safeAreaViewContent: {
		backgroundColor: '#ffffff',
		width: '100%',
		height: '100%'
	}

})