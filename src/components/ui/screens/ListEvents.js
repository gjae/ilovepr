import React, { Component } from 'react';
import { 
	View, 
	Text, 
	StyleSheet, 
	StatusBar, 
	FlatList, 
	TouchableHighlight, 
	Image, 
	ImageBackground, 
	ActivityIndicator, 
	RefreshControl,
	Alert
 } from 'react-native';
import { Row, Col, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import { Divider } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import {
	GET_SERVER_EVENTS
} from '../../../redux/actions/startScreens';

import env from '../../../config/env';
class ListEvents extends Component{

	static navigationOptions = ({navigation}) =>({
		headerStyle: {
			backgroundColor: '#FFFFFF'
		},
		title: navigation.state.params.title,

	})

	constructor(props){
		super(props);
		this.state = {
			events : [],
		};

		this._renderItem = this._renderItem.bind(this);
		this._keyExtractor = this._keyExtractor.bind(this);
	}


	_renderItem = ({item}) =>(


		<TouchableHighlight style={styles.listRows} onPress={()=> this.props.navigation.navigate("eventDetail", { event: item }) } >
			<Grid>
				<Row style={styles.rowHeader} > 
					<Col style={{ width: '17.5%' }}>
						<Image source={{ uri: env.base_url+'/'+item.organizador.img }} style={{ width: 50, height: 50, borderRadius: 30 }} />
					</Col>
					<Col>
						<Text style={styles.organizadorName}>Organizador por: {item.organizador.nombre}</Text>
					</Col>
				</Row>
				<Row stule={{ height: '10.2%', alignSelf: "center", alignItems: "center" }}>
					<Col>
						<Divider style={{ backgroundColor: '#EEEEEC' }} />
					</Col>
				</Row>
				<Row style={ styles.rowBody }>
					<Col style={{ width: '32%', paddingLeft: 4 }}>
						<ImageBackground source={{ uri : env.base_url+'/'+item.imagenes[0].imagen.ubicacion }} style={{ height:  "100%", width: '100%'}} />
					</Col>
					<Col  style={{ paddingLeft: 4 }} >
						<Row style={ { height: '30%' } } >
							<Col>
								<Text style={styles.titleEvent} >{item.nombre}</Text>
							</Col>
						</Row>
						<Row>
							<Col>
								<Text style={styles.textBody}>{item.descripcion}</Text>
							</Col>
						</Row>
						<Row>
							<Text style={styles.infoText}>
								<Icon name="md-calendar" size={22} color="#888A85" /> {item.fecha_hora}
								{'\n'}
								<Icon name="md-pin" size={22} color="#888A85" /> {item.direccion}
							</Text>
						</Row>	
					</Col>
				</Row>
			</Grid>
		</TouchableHighlight>
	)

	_keyExtractor = (item, index) => item.id

	componentDidMount(){
		this.props.dispatch({
			type: GET_SERVER_EVENTS,
			dispatch: this.props.dispatch,
			navigatioon: this.props.navigation,
			page: 1,
			buscar: this.props.navigation.state.params.buscar
		});
	}

	componentWillUnmount(){
		this.props.dispatch({type: "LIST_EVENT_UNMOUNT"});
	}

	_onTextPress(){
		this.props.navigation.navigate("events", {title: "Proximos eventos",buscar: "todo"}) ;
		this.props.dispatch({ type: "LIST_EVENT_UNMOUNT" });
		this.props.dispatch({
			type: GET_SERVER_EVENTS,
			dispatch: this.props.dispatch,
			navigatioon: this.props.navigation,
			page: 1,
			buscar: "todos"
		});
	}

	_renderData(){
		let { events } = this.state;
		if( events.length == 0 && this.props.load ){
			return(
				<View style={{ flex: 1, flexDirection: "column", justifyContent: "center" }}>
					<ActivityIndicator color={"#ef5350"}/>		
				</View>
			)
		}

		else if(events.length == 0 && this.props.load == false){
			return(
				<View style={{ flex: 1, flexDirection: "column", alignItems: "center", justifyContent: "center", height: "100%", width: "100%" }}>
					<Icon name="ios-happy-outline" size={250} color="#000" />
					<Text style={{ textAlign: "center" }} onPress={ ()=> this._onTextPress()}>
						Aún no tienes  
						<Text style={{color: "blue"}}> 
							{this.props.navigation.state.params.buscar == "marcados" ? " eventos marcados " : " eventos, intenta actualizar la lista "}
						</Text>
						
						{this.props.navigation.state.params.buscar == "marcados" ? "es un buen momento para revisar la lista de los proximos eventos." : ""}
					</Text>	
				</View>)
		}

		else{
			return <FlatList
					renderItem={ item => this._renderItem(item) }
					keyExtractor={ this._keyExtractor }
					extraData={this.state.events}
					data={this.state.events}
					onEndReachedThreshold={0.05}
					onEndReached={ e=>  {
						if( this.props.page <= this.props.total_pages ){
							this.props.dispatch({
								type: GET_SERVER_EVENTS,
								navigation: this.props.navigation,
								page: this.props.page,
								buscar: this.props.navigation.state.params.buscar
							})
						}
					} }
				/>
		}
	}

	componentWillReceiveProps(newProps){
		if( newProps.events.length > 0 ){
			let { events } = this.state;
			this.setState({ events: events.concat(newProps.events) });
			//Alert.alert("--", JSON.stringify(newProps))
			
		}

	}

	render(){
		return(
			<View style={styles.mainView}>
				<StatusBar
					translucent={false}
					backgroundColor={"#ef5350"}
				/>
				{this._renderData()}
			</View>
		)
	}

}

function mapDispatchToProps(dispatch, ownProps){
	return {
		dispatch: dispatch,
		navigation: ownProps.navigation
	}
}

function mapStateToProps(state){
	return {
		events: state.events.events,
		page: state.events.page,
		total_pages: state.events.total_pages,
		load: state.events.load
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ListEvents);

const styles = StyleSheet.create({
	mainView : {
		width: '100%',
		height: '100%',
		backgroundColor: '#EEEEEC'
	},
	listRows :{
		backgroundColor: '#FFFFFF',
		height: 200,
		width: '100%',
		marginTop: 7,
		elevation: 0.5
	},
	rowHeader :{
		paddingTop : 3,
		paddingLeft: 3,
		height: '32%',
		alignSelf: "center",
		alignItems: "center",
	},
	organizadorName :{
		fontSize: 19,
		fontFamily: 'NeutraText-Demi',
		color : "#000000"
	},
	rowBody :{
		alignSelf: "center",
		alignItems: "center",
		height: "68%"
	},
	titleEvent :{
		color: '#000000',
		fontSize: 19,
		fontFamily: 'NeutraText-Bold'
	}, 
	textBody :{
		color: "#888A85",
		fontFamily: 'NeutraText-Book',
		fontSize: 14
	},
	infoText :{
		color: "#888A85",
		fontFamily: 'NeutraText-Book',
		fontSize: 12
	}
})