import React from 'react';
import {
    View,
    StyleSheet,
    StatusBar,
    Text,
    FlatList,
    TouchableOpacity
} from 'react-native';
import { SafeAreaView } from 'react-navigation';

import { connect } from 'react-redux';
import Card from '../presentation/Card';

class Places extends React.Component{

    constructor(props){
        super(props);
    }


    render(){
        return(
            <SafeAreaView style={styles.mainView}>  
                <StatusBar
                    translucent={false}
                    backgroundColor={"#ef5350"}
                />
                <FlatList
                    data={this.props.hotels}
                    keyExtractor={ (item, index) => item.id }
                    renderItem={ ({item})=> 
                        <TouchableOpacity onPress={ () => this.props.navigation.navigate('placeDetails', { place: item, is: "hotels"  }) }> 
                            <Card image={{uri : item.images[0]}} name={item.name} location={item.location}   /> 
                        </TouchableOpacity> 
                    }
                    numColumns={2}
                    style={{marginTop: 5}}
                />

            </SafeAreaView>

        )
    }
}

Places.navigationOptions = (props)=> ({
    title: props.navigation.state.params.sideList
});

const styles = StyleSheet.create({
    mainView: {
        height: "100%",
        width: "100%",
        backgroundColor: "#ffffff"
    }
})

function mapStateToProps(state, ownProps){
    initials = [
        {
            id: 1,
            name: "Hotel San Carlos",
            location: "San Carlos",
            images: [
                "https://static1.squarespace.com/static/58ee8ad1f5e231091c285488/t/59c4e8fdcf81e05a81d9d555/1506076929029/Castillo-de-San-Cristobal.jpg",
                "https://static1.squarespace.com/static/58ee8ad1f5e231091c285488/t/59c4e8fdcf81e05a81d9d555/1506076929029/Castillo-de-San-Cristobal.jpg",
                "https://static1.squarespace.com/static/58ee8ad1f5e231091c285488/t/59c4e8fdcf81e05a81d9d555/1506076929029/Castillo-de-San-Cristobal.jpg"
            ],
            stars: 5
        },
        {
            id: 1,
            name: "Hotel 1",
            location: "San Carlos",
            images:[
             "https://static1.squarespace.com/static/58ee8ad1f5e231091c285488/t/59c4e8fdcf81e05a81d9d555/1506076929029/Castillo-de-San-Cristobal.jpg",
             "https://static1.squarespace.com/static/58ee8ad1f5e231091c285488/t/59c4e8fdcf81e05a81d9d555/1506076929029/Castillo-de-San-Cristobal.jpg",
             "https://static1.squarespace.com/static/58ee8ad1f5e231091c285488/t/59c4e8fdcf81e05a81d9d555/1506076929029/Castillo-de-San-Cristobal.jpg"
            ],
            stars: 5
        }
    ];
    return {
        hotels: initials
    }
}

export default connect(mapStateToProps, null)(Places);