import React, {Component} from 'react';
import { View, StyleSheet, TouchableOpacity, Alert, Text, StatusBar } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-navigation';


class ShopingCart extends Component {


	render(){
		return(
			<SafeAreaView style={styles.safeAreaViewContent}>
				<StatusBar
					translucent={false}
					backgroundColor={"#FFCB03"}
				/>
				<View>
					<Text>ShopingCart</Text>
				</View>
			</SafeAreaView>
		);
	}

}

export default connect(null, null)(ShopingCart);

const styles = StyleSheet.create({

	safeAreaViewContent: {
		backgroundColor: '#ffffff',
		width: '100%',
		height: '100%'
	}

});