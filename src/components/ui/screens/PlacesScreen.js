import React, { Component } from 'react';
import {
	View,
	StyleSheet,
	StatusBar,
	Alert,
	Text,
	ScrollView,
	ActivityIndicator,
	TouchableHighlight,
	FlatList,
	Image
} from 'react-native';

import { connect } from 'react-redux';
import { Row, Col, Grid } from 'react-native-easy-grid';
import { SafeAreaView } from 'react-navigation';
import ImageInnerShadow from '../presentation/ImageInnerShadow';
import {List, ListItem} from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import {
	GET_SERVER_PLACES
} from '../../../redux/actions/startScreens';
import env from '../../../config/env';

class PlacesScreen extends Component{

	static navigationOptions = ({navigation}) =>({
		title: navigation.state.params.tipo.split("_").join(" ")
	})

	constructor(props){
		super(props);
		this.state = {
			places: []
		};

		this._renderListItem = this._renderListItem.bind(this);
	}

	

	_renderListItem({item}){
		return(
			<TouchableHighlight style={styles.listItemStyle} onPress={()=> { this.props.navigation.navigate("place", { place: item }) }}>
				<Row style={{ width: '100%', paddingLeft: 27, paddingTop: 9 }} >
					<Col style={{ width: '19%' }}>
						<Image 
							source={{ uri: env.base_url+'/'+item.imagenes[0].imagen.ubicacion }} 
							style={{ width: 60, height: 60, borderRadius: 60 }}
						/>

					</Col>
					<Col>
						<Grid>
							<Row>
								<Col>
									<Text style={styles.titleTextItem}>{item.nombre}</Text>
								</Col>
							</Row>
							<Row>
								<Col style={{ width: '7%' }}>
									<Icon name="md-pin" size={24} color="#000000"/>
								</Col>
								<Col>
									<Text>{item.direccion}</Text>
								</Col>
							</Row>
							<Row>
								<Col>
									<Text>{item.estado_actual}</Text>
								</Col>
							</Row>
						</Grid>
					</Col>
				</Row>
			</TouchableHighlight>
		)

	}

	componentDidMount(){
		this.props.dispatch({
			type: GET_SERVER_PLACES, 
			data: {
				type: this.props.navigation.state.params.tipo
			},
			dispatch: this.props.dispatch,
			navigation: this.props.navigation,
			buscar : this.props.navigation.state.params.buscar
		});
	}

	componentWillReceiveProps(newProps){
		if( newProps.places.length > 0 ){
			let {places} = newProps;
			this.setState({ places });
		}
	}

	componentWillUnmount(){
		this.props.dispatch({type: "PLACES_SCREEN_UNMOUNT"});
	}

	_renderContent(){
		let { places } = this.state;
		if( places.length == 0 && this.props.load ){
			return(
				<View style={{ flex: 1, flexDirection: "column", justifyContent: "center" }}>
					<ActivityIndicator color={"#ef5350"}/>		
				</View>
			)
		}
		if( !this.props.hasData  && !this.props.load ){
			return(
				<View style={{ flex: 1, flexDirection: "column", alignItems: "center", justifyContent: "center", height: "100%", width: "100%" }}>
					<Icon name="ios-happy-outline" size={250} color="#000" />
					<Text style={{ textAlign: "center" }}>
						Lo sentimos, en este momento no hay información en esta categoría, trabajamos para incluir la 
						información que te damos a conocer
					</Text>	
				</View>)
		}

		else{
			return <FlatList
						data={this.state.places}
						extraData={this.state.places}
						renderItem={this._renderListItem}
					/>
		}
	}

	render(){
		let { places } = this.state;
		return(
			<SafeAreaView style={styles.safeArea}>
				<View style={styles.mainView}>
				<StatusBar
					translucent={false}
					backgroundColor={"#ef5350"}
				/>
					{this._renderContent()}
				</View>
			</SafeAreaView>	
		)
	}

}

function mapStateToProps(state, ownProps){
	return {
		navigation: ownProps.navigation,
		places: state.places.places,
		load: state.places.load,
		hasData: state.places.places.length > 0
	}
}

export default connect(mapStateToProps, null)(PlacesScreen);

const styles = StyleSheet.create({

	safeArea :{
		backgroundColor: '#FFFFFF'
	},


	mainView :{
		width: '100%',
		height: '100%',
		backgroundColor: '#ffffff'
	},

	listItemStyle :{
		height: 75,
		width: '100%',
		borderBottomWidth: 1,
		borderBottomColor: '#f3f3f3'
	},
	titleTextItem :{
		fontSize: 18,
		fontWeight: "bold", 
		color: "#000000"
	}

})
