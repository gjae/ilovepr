import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import ImageSlider from 'react-native-image-slider';
import Icon from 'react-native-vector-icons/Ionicons';
import Divider from '../presentation/Divider'
import HotelComments from '../presentation/HotelComments';

export default class HotelDetails extends React.Component{

    static navigationOptions =({navigation}) =>({
        title: "Detalles del hotel"
    })

    constructor(props){
        super(props);
        this.state ={
            modalVisible: false
        }
    }

    _renderStars = (count) => {
        if( true ){
            let stars = [];
            for(var i = 0; i< count; i++){
                stars.push(
                    <Icon name="md-star" color="#EDD400" size={18} />
                )
            }
            return stars;
        }

        return null;
    }

    _onModalRequestClose =(modal) =>{
        this.setState({
            modalVisible: modal.isOpen
        })
    }

    render(){
        return(
            <SafeAreaView style={{ width: "100%", flex: 1, flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
                <View style={{ width: "100%", height: "45%" }}>
                        
                    <ImageSlider 
                        loopBothSides
                        autoPlayWithInterval={3000}
                        images={this.props.navigation.state.params.imagenes}
                        style={{height: "80%"}}
                    />
                </View>
                <View style={{ width: "95%", flex: 1, flexDirection: "column", marginTop: 7 }}>
                    <View style={{ backgroundColor: "#ef5350", height: "20%", paddingLeft: 8, paddingTop: 4, alignItems: "center"}} >
                        <Text style={{ color: "#ffffff", fontSize: 20 }}>
                           {this.props.navigation.state.params.hotel.nombre}
                        </Text>
                        <Text style={{ color: "#ffffff", fontSize: 14 }}>
                           <Icon name="md-pin" color="#ffffff" size={19} />  
                           {' '+this.props.navigation.state.params.hotel.direccion}
                        </Text>
                    </View>
                    <View style={{ backgroundColor: "#ffffff", height: "79%" }}>
                        <ScrollView style={{ flex: 1 , flexDirection: "column"}}>
                            <View style={{ flex:1, flexDirection: "row", justifyContent: "center" }}>
                                 { this.props.navigation.state.params.hotel.hotel.wifi == 1 ? <Icon name="ios-wifi" color="#000000" size={33} /> :null }
                                <Text>{' '}</Text>
                                { this.props.navigation.state.params.hotel.hotel.taxi == 1 ? <Icon name="md-car" color="#000000" size={33} /> :null }
                                <Text>{' '}</Text>
                                { this.props.navigation.state.params.hotel.hotel.taxi == 1 ? <Icon name="md-restaurant" color="#000000" size={33} /> :null }
                                <Text>{' '}</Text>
                                { this.props.navigation.state.params.hotel.hotel.taxi == 1 ? <Icon name="md-shirt" color="#000000" size={33} /> :null }
                            </View>
                            <View style={{marginTop: 10, flex: 1, flexDirection: "row", justifyContent: "center", paddingLeft: 5, paddingRight: 5, height: 100 }}>
                                <View style={{ width: "50%", height: 100 ,flex: 1, flexDirection: "column", justifyContent:"center", alignItems:"center" }}>
                                    <Text style={{ color:"#ef5350", fontSize: 30 }}>
                                        8,5
                                    </Text>
                                    <Text style={{ color:"#ef5350", fontSize: 20 }}>
                                        Recomendado
                                    </Text>
                                </View>
                                <View style={{ backgroundColor:"#ef5350", width: "50%", height: 100 ,flex: 1, flexDirection: "column", justifyContent:"center", alignItems:"center" }}>
                                    <Text style={{ color:"#ffffff", fontSize: 30 }}>
                                        360 $
                                    </Text>
                                    <Text style={{ color:"#ffffff", fontSize: 20 }}>
                                        Costo por día
                                    </Text>
                                </View>
                            </View>
                            <Divider />
                            <View style={{flex: 1, flexDirection: "row", justifyContent: "center"}}>
                                <TouchableOpacity onPress={()=> this.setState({modalVisible: true})}>
                                    <Text style={{ color:"#000000", fontSize: 15 }}>
                                        Mira las opiniones de otros usuarios
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </View>  
                <HotelComments 
                    id={this.props.navigation.state.params.hotel.id} 
                    comentarios={this.props.navigation.state.params.comentarios} 
                    visible={this.state.modalVisible} onRequestClose={()=> this._onModalRequestClose} 
                />                  
            </SafeAreaView>
        )
    }
}