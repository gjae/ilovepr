import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Grid, Row , Col } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import { Divider } from 'react-native-elements';


const PlaceInfo = ({ place }) => (
	<Grid style={styles.gridView}>
		<Row style={{ height: '15%' }}>
			<Col style={{ width: "10%", paddingTop: 4 }}>
				<Icon name="md-call" color="#000000" size={32} />
			</Col>
			<Col style={styles.colInfo}>
				<Text style={styles.textInfo}> {place.phone} </Text>
			</Col>
		</Row>
		<Row style={{ height: "4%" }}>
			<Col style={{ paddingLeft: '7%' }}>
				<Divider style={{backgroundColor: '#D3D7CF'}} />
			</Col>
		</Row>
		<Row style={{ height: '15%' }}>
			<Col style={{ width: "10%", paddingTop: 4 }}>
				<Icon name="md-pin" color="#000000" size={32} />
			</Col>
			<Col style={styles.colInfo}>
				<Text style={styles.textInfo}> {place.direccion} </Text>
			</Col>
		</Row>
		<Row style={{ height: "4%" }}>
			<Col style={{ paddingLeft: '7%' }}>
				<Divider style={{backgroundColor: '#D3D7CF'}} />
			</Col>
		</Row>
		<Row style={{ height: '15%' }}>
			<Col style={{ width: "10%", paddingTop: 4 }}>
				<Icon name="md-mail" color="#000000" size={32} />
			</Col>
			<Col style={styles.colInfo}>
				<Text style={styles.textInfo}> {place.email} </Text>
			</Col>
		</Row>
		<Row style={{ height: "4%" }}>
			<Col style={{ paddingLeft: '7%' }}>
				<Divider style={{backgroundColor: '#D3D7CF'}} />
			</Col>
		</Row>
	</Grid>
);

export default PlaceInfo;

const styles = StyleSheet.create({
	gridView :{
		paddingLeft: '3%',
		paddingTop: '3%'
	},
	textInfo :{
		color: "#000000",
		fontSize: 17
	},
	colInfo :{
		paddingTop: "3%"
	}
})