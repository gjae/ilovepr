import PlaceInfo from './PlaceInfo';
import PlacePhotos from './PlacePhotos';
import PlaceComments from './PlaceComments';



export {
	PlaceInfo,
	PlacePhotos,
	PlaceComments

};