import React, { Component } from 'react';
import { View, Text, StyleSheet, Alert, Image, ScrollView } from 'react-native';
import { Grid, Row , Col } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import { Divider } from 'react-native-elements';
import PhotoGrid from 'react-native-thumbnail-grid/PhotoGrid';

import ImageView from 'react-native-image-view';
import env from '../../../../config/env';

class PlacePhotos extends Component {


	constructor(props){
		super(props);
		this.state = {
			place : "",
			images: [],
			imageViewVisible: false,
			imageSource: ""
		}

		this._renderGrid = this._renderGrid.bind(this);
		this._onPressImage = this._onPressImage.bind(this);
		this._onClose = this._onClose.bind(this);
	}

	componentDidMount(){
		let { imagenes } = this.props.place;
		let images = [];
		for(let i = 0; i < imagenes.length; i++){
			images.push(env.base_url+'/'+imagenes[i].imagen.ubicacion);
		}
		this.setState({images})
	}

	_renderGrid = () => {
		if( this.state.images.length > 0 ){
			return
		}

		return null;
	}

	_onPressImage = (uri) =>{
		this.setState({
			imageViewVisible: true,
			imageSource: uri
		});
	}
	_onClose = () => {
		this.setState({
			imageViewVisible: false
		});
	}

	render(){
		return (
			<ScrollView>
				 <PhotoGrid 
				 	onPressImage={ (event, source) => this._onPressImage(source) } 
				 	style={{ width: '100%', height: '100%' }} 
				 	source={this.state.images} 
				 />
				 <ImageView 
				 	animationType="fade" 
				 	images={[{  source: { uri: this.state.imageSource } }]} 
				 	isVisible={this.state.imageViewVisible} 
				 	onClose={this._onClose}
				 />
			</ScrollView>
		)
	}

}

export default PlacePhotos;