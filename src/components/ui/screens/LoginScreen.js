import React, { Component } from 'react';
import {
	View,
	Text,
	StatusBar,
	StyleSheet,
	ImageBackground,
	Alert
} from 'react-native';

import {
	Button,
	Divider
} from 'react-native-elements';

import {
	Container,
	Row
} from 'react-native-easy-grid'

import { connect } from 'react-redux';
import {
	PERSIST_LANG
} from '../../../redux/actions/startScreens';
import { 
	LoginManager,  
	GraphRequest,
	GraphRequestManager ,
	AccessToken
} from 'react-native-fbsdk';
import { START_SESSION, LOAD_SESSION, IS_LOAD_SESSION } from '../../../redux/actions/startScreens';


class LoginScreen extends Component{

	constructor(props){
		super(props);
		this.state = {
			fb_permissions: ["public_profile", "email", 'user_location', 'user_hometown'],
			inLoad: false
		}
	}

	dispatchToken(){
		AccessToken.getCurrentAccessToken()
		.then(data =>{
			this.props.dispatch({
				type: START_SESSION, 
				data: { method: "FACEBOOK", token: data.accessToken.toString() },
				navigation: this.props.navigation,
				dispatch: this.props.dispatch
			});
		});		
	}

	handleFacebookLogin =  () => {
		this.props.dispatch({type:IS_LOAD_SESSION});
		LoginManager.logInWithReadPermissions(this.state.fb_permissions).then((result)=>{
			if(!result.isCancelled) this.dispatchToken();
			//this.setState({isLoad: false});
		}).catch(e=>{
			if( e.code == "EUNSPECIFIED" ){
				LoginManager.logOut();
				LoginManager.logInWithReadPermissions(this.state.fb_permissions).then( result =>{
					if(!result.isCancelled) this.dispatchToken();
				});
			}
		});
	}

	render(){
		return(
			<View style={styles.container} >
				<StatusBar
					translucent
					backgroundColor={"transparent"}
				/>
				<ImageBackground source={require('../assets/img/login_background.jpg')} style={[styles.container, styles.backgroundImgContainer]}>
					<Button disabled={this.props.isLoad} onPress={()=>{ this.handleFacebookLogin() }}  title={ !this.props.isLoad ?"Login con facebook":'Cargando...'}  style={styles.buttonLogin}  backgroundColor="#204A87"  buttonStyle={{marginBottom: 12}} />
					
				</ImageBackground>
			</View>
		);
	}

}
//<Button title="Login telefono"  style={styles.buttonLogin}  backgroundColor="#EF2929"  />
function mapDispatchToProps(dispatch){
	return {
		dispatch: (action) => {
			dispatch( { type: PERSIST_LANG, action: { lang: "es" } } );
		},
		dispatch
	}
}

function mapStateToProps(state, ownProps){
	return {
		lang: state.lang.lang,
		navigation: ownProps.navigation,
		isLoad: state.user.isLoadSession
	}
}

export default connect( mapStateToProps, null )(LoginScreen);

const styles = StyleSheet.create({

	container: {
		width: "100%",
		height: '100%'
	},
	buttonLogin : {
		backgroundColor: '#204A87',
		marginTop: "80%"
	},
	backgroundImgContainer: {
		paddingTop: "72%"
	}

})