import React , { Component } from 'react';
import {
	SafeAreaView
} from 'react-navigation';
import {
	Col,
	Row,
	Grid
} from 'react-native-easy-grid';

import {
	View,
	Text,
	StyleSheet,
	StatusBar,
	TouchableOpacity,
	ScrollView,
	Image,
	TouchableHighlight
} from 'react-native';

import Header from '../presentation/HeaderNavigation';
import RoundIcon from '../presentation/RoundIcon';
import Icon from 'react-native-vector-icons/Ionicons';
import i18n from '../../../config/translation/i18n'
import {connect} from 'react-redux';

class CategoriesScreen extends Component{

	static navigationOptions = ({navigation}) => ({
		headerStyle: {
			elevation: 0
		},
		headerLeft: ()=> null,
		title: "¿Que hacemos hoy?",
		header: ()=> <Header navigation={navigation} />
	})

	render(){
		 return(

			<SafeAreaView style={styles.safeAreaViewContent}>
				<StatusBar
					translucent={true}
				/>
				<ScrollView style={{  }}>
					<TouchableOpacity onPress={()=>{ this.props.navigation.navigate("places", {tipo: "Lugares", buscar: "todos"}) }} >
						<Row style={[styles.rowCategories, {backgroundColor: '#4E9A06'} ]} >
							<Col style={styles.colIcon}> 
								<RoundIcon>
									<Icon name="ios-navigate-outline" size={45} color={"#FFFFFF"} />
								</RoundIcon>
							</Col>
							<Col style={styles.colTextArea} >  
								<Text style={styles.textScollArea} >{ i18n.t("places") }</Text>
							</Col>
						</Row>
						</TouchableOpacity>
					<TouchableOpacity onPress={ ()=> this.props.navigation.navigate("events", {title: "Proximos eventos",buscar: "todo"}) }>
						<Row style={[styles.rowCategories, {backgroundColor: '#75507B'} ]} >
							<Col style={styles.colIcon}> 
								<RoundIcon>
									<Icon name="ios-calendar-outline" size={45} color={"#FFFFFF"} />
								</RoundIcon>
							</Col>
							<Col style={styles.colTextArea} >  
								<Text style={styles.textScollArea} >{ i18n.t("event") }</Text>
							</Col>
						</Row>
						</TouchableOpacity>
					<TouchableOpacity onPress={()=>{ this.props.navigation.navigate("places", {tipo: "RESTAURANTE", buscar: "todos"}) }}>
						<Row style={[styles.rowCategories, {backgroundColor: '#E9B96E'} ]} >
							<Col style={styles.colIcon}> 
								<RoundIcon>
									<Icon name="md-restaurant" size={45} color={"#FFFFFF"} />
								</RoundIcon>
							</Col>
							<Col style={styles.colTextArea} >  
								<Text style={styles.textScollArea} >{ i18n.t("restaurant") }</Text>
							</Col>
						</Row>
						</TouchableOpacity>
					<TouchableOpacity onPress={()=> this.props.navigation.navigate('hotels', {sideList: i18n.t("hotel") })}>
						<Row style={[styles.rowCategories, {backgroundColor: '#FA5C5C'} ]} >
							<Col style={styles.colIcon}> 
								<RoundIcon>
									<Icon name="ios-briefcase-outline" size={45} color={"#FFFFFF"} />
								</RoundIcon>
							</Col>
							<Col style={styles.colTextArea} >  
								<Text style={styles.textScollArea} >{ i18n.t("hotel") }</Text>
							</Col>
						</Row>
						</TouchableOpacity>
					<TouchableOpacity onPress={()=>{ this.props.navigation.navigate("places", {tipo: "SUPER_MERCADO", buscar: "todos"}) }}>
						<Row style={[styles.rowCategories, {backgroundColor: '#729FCF'} ]} >
							<Col style={styles.colIcon}> 
								<RoundIcon>
									<Icon name="ios-cart-outline" size={45} color={"#FFFFFF"} />
								</RoundIcon>
							</Col>
							<Col style={styles.colTextArea} >  
								<Text style={styles.textScollArea} >{ i18n.t("market") }</Text>
							</Col>
						</Row>
						</TouchableOpacity>
					<TouchableOpacity onPress={()=>{ this.props.navigation.navigate("places", {tipo: "MALL", buscar: "todos"}) }}>
						<Row style={[styles.rowCategories, {backgroundColor: '#888A85'} ]} >
							<Col style={styles.colIcon}> 
								<RoundIcon>
									<Icon name="ios-basket-outline" size={45} color={"#FFFFFF"} />
								</RoundIcon>
							</Col>
							<Col style={styles.colTextArea} >  
								<Text style={styles.textScollArea} >{ i18n.t("mall") }</Text>
							</Col>
						</Row>
						</TouchableOpacity>

				</ScrollView>
			</SafeAreaView>

		);
	}

}


export default CategoriesScreen;

const styles = StyleSheet.create({

	safeAreaViewContent :{
		backgroundColor: '#ffffff',
		width: '100%',
		height: '100%',
		
	},
	rowCategories: {
		height: "20%",
		width: '100%',
		paddingTop: "2%",
		paddingBottom: '2%',
		paddingLeft: '2%',
		paddingRight: '2%'
	},
	scroll:{
		width: '100%', 
		height: '100%', 
		marginTop: '10%',
		paddingLeft: 10
	},
	colIcon :{
		width: '25.4%'
	},
	textScollArea :{
		fontSize: 25,
		fontWeight: 'bold' ,
		color:"#ffffff"
	},
	colTextArea :{
		paddingTop: "4%"
	}
});