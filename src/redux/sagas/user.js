import { takeEvery,  put } from 'redux-saga/effects';
import { Account } from '../../config/storage';
import { START_SESSION, IS_LOAD_SESSION, FINISH_SESSION_WITHOUT_ERROR, FINISH_SESSION_WITH_ERROR } from '../actions/startScreens';
import { Alert, Linking } from 'react-native';
import { GraphRequest, GraphRequestManager, AccessToken } from 'react-native-fbsdk';
import env from '../../config/env';
const moment = require('moment');

async function fetchAuth({ accessToken, data, dispatch, navigation }){
  //  action.dispatch({type: IS_LOAD_SESSION});
    let bodyRequest = {
        name: data.first_name+' '+data.last_name,
        email: data.email,
        fb_token: data.id,
        social_network: "FACEBOOK",
        pais_id: 0,
        pais_location_id: 0
    }
    let response = await fetch(env.api_url+'/auth', {
        method: "POST",
        headers: {
            'Content-Type': "application/json",
            Accept: "application/json"
        },
        body: JSON.stringify(bodyRequest)
    }).catch(e =>{
        Alert.alert("Error", "No se ha podido cargar la información con el servidor, verifique su conexion e intente de nuevo");
        dispatch({type: FINISH_SESSION_WITH_ERROR})
    });
    let responseJson = JSON.parse(response._bodyInit);
    Account.put({
        _id: "account",
        data:{
            token: responseJson.token,
            type: responseJson.type
        },
        document_at: moment(new Date).format('YYYY-MM-DD HH:MM:SS')
    }).then(r =>{
        navigation.navigate('categorias');
        dispatch({ type: FINISH_SESSION_WITHOUT_ERROR, token: responseJson.token });
    })
}

function* store(action){
    try{
        let { method, token } = action.data;
        AccessToken.getCurrentAccessToken()
        .then(accessToken => {
            const responseGraph = (err, result )=>{
                if( !err ){
                    Account.get('account')
                    .then(async data => {
                        return await Account.remove(data)
                    } )
                    .then(async data => {
                        fetchAuth({accessToken, data: result, dispatch: action.dispatch, navigation: action.navigation})
                    })
                    .catch( async e =>{
                        fetchAuth({accessToken, data: result, dispatch: action.dispatch,navigation: action.navigation});
                    });
                }
            }
            let request = new GraphRequest('/me', {
                parameters: {
                    fields:{
                        string: "email, location, hometown, first_name, last_name, id"
                    }
                }
            } , responseGraph);

            new GraphRequestManager().addRequest(request).start();
        });
        
    }catch(err){
        Alert.alert("--", JSON.stringify(err))
    }

}


function* user(action){

    yield takeEvery(START_SESSION, store);

}

export default user;