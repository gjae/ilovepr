import { takeEvery, put, select } from 'redux-saga/effects';
import {
    GET_SERVER_PLACES,
    SERVER_PLACES_ERROR,
    SERVER_PLACES_SUCCESS,
    GET_SERVER_EVENTS,
    SERVER_EVENTS_SUCCESS,
    MARK_PLACE,
    DISMARK_PLACE,
    GET_SERVER_HOTELS,
    SERVER_HOTELS_ERROR,
    SERVER_HOTELS_SUCCESS,
    SENT_COMMENT_PLACE,
    RESPONSE_COMMENT_PLACE
} from '../actions/startScreens';
import { Alert }from 'react-native';
import env from '../../config/env';
import { Account } from '../../config/storage';

const getStateItems = state => state;

function* getFromServer(resource = '', action, method = "GET", body = {} ){
    //Alert.alert("..", body);
    let token = yield Account.get('account');
    let requestData = {
        method,
        headers:{
            Accept: "application/json",
            Authorization: `bearer ${token.data.token}`,
            'Content-Type': "application/json"
        }
    };

    if( typeof(body.lugar_id )!= "undefined" ) requestData.body = JSON.stringify({...body});
    let response = yield fetch(env.api_url+'/'+resource, requestData)
    .catch(e =>{
        Alert.alert("Error", "Estamos presentando errores de conexion al servidor, verifique su conexion a internet");
        action.navigation.goBack();
    });

    return JSON.parse(response._bodyInit);

}

function* get(action){
    let state = yield select();
    let data = yield getFromServer('places?tipo='+action.data.type+'&ciudad='+state.places.ciudad, action);
    
    if( action.buscar == "marcados" ){
        data.lugares = data.lugares.filter((data)=>{
            return data.marcado != null;
        });
    }

    yield put({type: SERVER_PLACES_SUCCESS, places: data.lugares});
}

function* getEvents(action){
    let state = yield select();
    let response = yield getFromServer('events?page='+action.page+'&buscar='+action.buscar+'&ciudad='+state.places.ciudad, action);
    if( action.buscar == "marcados" ){
        response.events = response.events.filter((event)=>{
            if( event.marcado != null && ( event.marcado.quiero_asistir != null || event.marcado.marcado != null ) )
                return true;
            return false;
        })
    } 
    yield put({
        type: SERVER_EVENTS_SUCCESS,
        data:{
            events: response.events,
            page: action.page,
            total_pages: response.total_pages,
            load: false
        }
    })
}

function* mark(action){
    
    let response = yield getFromServer(action.data.resource, action, "POST", action.data);
    
    if( action.data.to == "dismark" ) yield put({type: DISMARK_PLACE, data : { id: action.data.lugar_id } });
}


function* getHotels(action){
    let response = yield getFromServer( "hotels", action, "GET" );
    //Alert.alert("--", JSON.stringify(response));
    yield put({
        type: SERVER_HOTELS_SUCCESS,
        data: {
            hotels: response.hotels
        }
    });
}

function* sentCommentPlace(action){
    let response = yield getFromServer('hotels/comment', action, 'POST', action.data);
    yield put({ type: RESPONSE_COMMENT_PLACE, data: { comment: response.comment, user: response.usuario } });
}

function* getAllPlaces(action){
    yield response = yield getFromServer(`places/get?position=${action.data.longitud},${action.data.latitud}`, action);
    yield put({ type: SERVER_PLACES_SUCCESS, places: response.lugares });

}

function* getCiudades(action){
    let ciudades = yield getFromServer('ciudades', action);
    yield put({
        type: "SET_CIUDADES_ARRAY",
        ciudades: ciudades.ciudades
    })
}

export default function* server(action){
    yield takeEvery(GET_SERVER_PLACES, get);
    yield takeEvery(GET_SERVER_EVENTS, getEvents);
    yield takeEvery(MARK_PLACE, mark);
    yield takeEvery(GET_SERVER_HOTELS, getHotels);
    yield takeEvery(SENT_COMMENT_PLACE, sentCommentPlace);
    yield takeEvery(GET_SERVER_PLACES+"_ALL", getAllPlaces);
    yield takeEvery("GET_CIUDADES", getCiudades);
}