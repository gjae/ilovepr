import { all } from 'redux-saga/effects';
import {
	settings
} from './settings';
import account from './user';
import server from './server';
 


function* rootSaga(){
	yield all([
		settings(),
		account(),
		server()
	]);
}

export default rootSaga;