import { put, takeEvery, all, call } from 'redux-saga/effects';
import {
	Alert
} from 'react-native';
import { SET_DEFAULT_LANG, PERSIST_LANG, VERIFY_SETTINGS, SAVE_NEW_SETTINGS, START_SESSION } from '../actions/startScreens';
import { Settings, Account } from '../../config/storage';
import moment from 'moment';

function* removeSettings( settings, action ){
	yield Settings.remove(settings);
	yield putSettings( action );
}


function* putSettings( action ){

	yield Settings.put({
		_id: "settings",
		content: action.content,
		last_modify: moment()
	});

	yield put({ type: SET_DEFAULT_LANG, lang: action.content.lang });

	action.navigation.navigate("start");
}

function* settingsGenerator(  action ){

		// SETEANDO LA CONFIGURACION INTERNA DE LA APLICACION 
		// DE MANERA OFFLINE
		// USANDO POUCHDB: PIMERO INTENTA BUSCAR LA CONFIGURACION (SETTINGS) DE LA APLICACION,
		// SI LA ENCUENTRA LA REMUEVE Y VUELVE A CREARLA
	try{
		let  setts = yield Settings.get("settings");
		yield removeSettings(setts, action);
	}catch( error ){
		yield putSettings( action );
	}
}

/**
 * VERIFICA SI EL USUARIO YA POSEE UNA CONFIGURACION ESTABLECIDAD,
 * SI EL USUARIO NO POSEE UNA CONFIGURACION LO ENVIA A LA PANTALA DE SELECCION DE IDIOMA
 * (DE LO CONTRARIO LANZA UNA EXCEPCION QUE CAPTURA EL CATCH Y LO ENVIA A SELECCIONAR IDIOMA)
 * SI TIENE UNA CONFIGURACION DE IDIOMA ESTABLECIDA ENTONCES VERIFICA QUE EL USUARIO
 * TENGA UNA CUENTA ACTIVA, DE TENERLA ENVIA UN DISPATCH PARA QUE SE REFRESQUE EL TOKEN DEL SERVIDOR
 * 
 * @param action objeto action de la saga
 */
function* verifySettings(action){

	try{
		let settings = yield Settings.get('settings');

		put({type: SET_DEFAULT_LANG, lang: settings.content.lang });
		Account.get('account')
		.then( r => {
			action.dispatch({
				type: START_SESSION, 
				data: {token: '', method: "FACEBOOK"}, 
				dispatch: action.dispatch, 
				navigation: action.navigation 
			})
		})
		.catch(r =>{
			action.navigation.navigate("start");
		});

	}catch( error ){
		action.navigation.navigate("config");
	}
}

export function* settings(){
	yield takeEvery( PERSIST_LANG, settingsGenerator );
	yield takeEvery( VERIFY_SETTINGS, verifySettings );
}

