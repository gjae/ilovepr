import { 
	SET_LANG, 
	SET_DEFAULT_LANG,
	FINISH_SESSION_WITHOUT_ERROR, 
	IS_LOAD_SESSION, 
	FINISH_SESSION_WITH_ERROR 
} from '../actions/startScreens';
import {
	Alert
} from 'react-native';

function user(state = {token: '', isLoadSession: false}, action){
	
	switch(action.type){
		case FINISH_SESSION_WITHOUT_ERROR:
			//Alert.alert("--", "PRUEBA");
			return {token: action.token, isLoadSession: false};
		break;
		case IS_LOAD_SESSION:
			return { token: '', isLoadSession: true }
		break;
		case FINISH_SESSION_WITH_ERROR:
			return { token: '', isLoadSession: false };
		break;
		default:
			return state;
		break;
	}
}

function lang(state = {lang: "es"}, action){

	switch( action.type ){
		case SET_DEFAULT_LANG: {
			//Alert.alert("REDUCER", SET_DEFAULT_LANG);
			return Object.assign({}, state, {
				lang: action.lang
			});

			break;
		}

		default : {
			return state;
		}
	}

} 


export {
	lang,
	user
}