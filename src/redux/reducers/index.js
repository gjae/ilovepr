import { combineReducers } from 'redux';
import { lang, user } from './startReducerScreen';
import { places, events, hotels, comment } from './server';

export default combineReducers({
	lang,
	user,
	places,
	events,
	hotels,
	comment
});