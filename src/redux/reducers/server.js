import {
    SERVER_PLACES_ERROR,
    SERVER_PLACES_SUCCESS,
    SERVER_EVENTS_ERROR,
    SERVER_EVENTS_SUCCESS,
    DISMARK_PLACE,
    SERVER_HOTELS_ERROR,
    SERVER_HOTELS_SUCCESS,
    RESPONSE_COMMENT_PLACE
} from '../actions/startScreens';
import {Alert} from 'react-native';

export function events( state = { events: [], load: true, page: 1, total_pages: 0 }, action ){
    switch( action.type ){
        case SERVER_EVENTS_SUCCESS:
            return Object.assign({}, state, {
                events: action.data.events,
                page: action.data.page + 1,
                total_pages: action.data.total_pages,
                load: action.data.load
            });
        break;
        case "LIST_EVENT_UNMOUNT":
            return Object.assign({}, state, {load: true});
        break;
        default :
            return state;
        break;
    }
}

export function places(state ={load: true, places: [], favoriteIcon: "ios-heart-outline", ciudad: "todas", ciudades: []}, action){

    switch(action.type){
        case SERVER_PLACES_SUCCESS:
            return Object.assign({}, state, {
                load:false,
                places: action.places
            });
        break;
        case DISMARK_PLACE:
            return Object.assign({}, state, {
                load: false,
                places: state.places.map((data)=>{
                    if( data.id == action.data.id ) data.marcado = null;
                    return data;
                })
            })
        break;
        case "PLACES_SCREEN_UNMOUNT":
            return Object.assign({}, state, { load: true });
        break;
        case  "SET_CIUDAD":
            return {...state, ciudad: action.ciudad};
        break;
        case "SET_CIUDADES_ARRAY":
            return {...state, ciudades: action.ciudades}
        break;
        default :
            return state;
        break;
    }

}

export function comment(state = { newComment: null, user: null }, action){
    switch(action.type){
        case RESPONSE_COMMENT_PLACE:
            return Object.assign({}, state, { newComment: action.data.comment, user: action.data.user });
        break;
        case "COMMENT_SAVED":
            return Object.assign({}, state, {newComment: null, user: null});
        break;
        default:
            return state;
        break;
    }
}

export function hotels(state = {hotels: [], load: true}, action){

    switch(action.type){
        case SERVER_HOTELS_SUCCESS:
            return Object.assign({}, state, {
                load: false,
                hotels: action.data.hotels
            });
        break;
        default: 
            return state;
        break;
    }

}