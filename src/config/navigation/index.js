import React from 'react';
import { createStackNavigator } from 'react-navigation';
import startApp from './startScreens';
import Splash from '../../components/ui/screens/SplashScreen';
import InitialAppConfig from '../../components/ui/screens/InitialAppConfig';
import MainTab from './MainTabNavigation';

export default createStackNavigator({
	splash: {
		screen: Splash,
		navigationOptions: {
			header:() => null
		}
	},
	config: {
		screen: InitialAppConfig,
		navigationOptions: {
			header:() => null
		}
	},
	start: {
		screen: startApp,
		navigationOptions: {
			header: () => null
		}
	},
	postLogin: {
		screen: MainTab
	}
},{

	navigationOptions: {
		header: () => null
	}
})