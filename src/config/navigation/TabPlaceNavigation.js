import React, { Component } from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation';
import { PlaceInfo, PlacePhotos, PlaceComments } from '../../components/ui/screens/PlaceTabsDetails';


export default createMaterialTopTabNavigator({

	informacion :{
		screen: props => <PlaceInfo {...props.screenProps} />
	},
	imagenes :{
		screen: props=> <PlacePhotos {...props.screenProps} />
	},
/*	comentarios :{
		screen: PlaceComments
	} */

}, {
	tabBarOptions: {
		style:{
			backgroundColor: '#ef5350',
			elevantion: 0
		},
		labelStyle:{
			fontSize: 13,
			fontWeight: 'bold' ,
			fontFamily: 'NeutraText-Bold'
		},
		indicatorStyle :{
			backgroundColor: '#FFFFFF'
		}
	}
});
