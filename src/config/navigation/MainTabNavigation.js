import React from 'react';
import { createMaterialTopTabNavigator, createStackNavigator } from 'react-navigation';
import CategoriesScreen from '../../components/ui/screens/CategoriesScreen';
import ShopingCart from '../../components/ui/screens/ShopingCart';
import MenuDiario from '../../components/ui/screens/MenuDiario';
import ShopingCartHeaderIcon from '../../components/ui/presentation/ShopingCartHeaderIcon';



import { StyleSheet } from 'react-native';

const TopTab = createMaterialTopTabNavigator({

	Categorias: {
		screen: CategoriesScreen,
		navigationOptions: {
			header: ()=> null
		}
	},
	MenuDiario: {
		screen: MenuDiario,
		navigationOptions:{
			title: "MENU DIARIO"
		}
	}

}, {
	tabBarOptions: {
		style:{
			backgroundColor: '#FFE003',
		},
		labelStyle:{
			fontSize: 15,
			fontWeight: 'bold' ,
			fontFamily: 'NeutraText-Bold'
		}
	}
});

export default createStackNavigator({
	mainTab: {
		screen: TopTab,
		navigationOptions:{
			title: "Bienvenido",
		}
	},
	cart: {
		screen: ShopingCart
	}
}, {
	navigationOptions: ({navigation}) => ({
		headerStyle: {
			backgroundColor: '#FFE003',
			elevation: 0
		},
		headerTitleStyle:{
			color: "#ffffff",
			fontFamily: 'NeutraText-Bold'
		},
		headerRight:( <ShopingCartHeaderIcon navigation={navigation} /> ),
		headerTintColor: "#FFFFFF"
	})
});


const styles = StyleSheet.create({

	stackHeader: {
		backgroundColor: '#FFCB03'
	}
	
})