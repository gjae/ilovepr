import React from 'react';
import { createStackNavigator } from 'react-navigation';

import CategoriesScreen from '../../components/ui/screens/CategoriesScreen';

import LoginScreen from '../../components/ui/screens/LoginScreen';
import PlacesScreen from '../../components/ui/screens/PlacesScreen';
import PlaceScreen from '../../components/ui/screens/PlaceScreen';
import TabPlaceNavigation from './TabPlaceNavigation';
import Events from '../../components/ui/screens/ListEvents';
import EventDetail from '../../components/ui/screens/EventDetails';
import SidesPlaces from '../../components/ui/screens/Places';
import PlaceDetails from '../../components/ui/screens/PlaceDetails';
import Hotels from '../../components/ui/screens/Hotels';
import HotelDetails from '../../components/ui/screens/HotelDetails';
import MapScreen from '../../components/ui/screens/MapScreen';

export default createStackNavigator({
	login: {
		screen: LoginScreen,
		navigationOptions: {
			header:() => null
		}
	},
	categorias:{
		screen: CategoriesScreen
	},
	places: {
		screen: PlacesScreen
	},
	place: {
		screen: PlaceScreen
	},
	events: {
		screen: Events
	},
	eventDetail: {
		screen: EventDetail
	},
	sides:{
		screen: SidesPlaces
	},
	placeDetails: {
		screen: PlaceDetails
	},
	hotels:{
		screen: Hotels
	},
	hotelDetails: {
		screen: HotelDetails
	},
	map:{
		screen: MapScreen
	}


}, {

	nvigationOptions: {
		header:()=> null
	}

});