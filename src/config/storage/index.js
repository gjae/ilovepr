import PouchDB from 'pouchdb-core';
PouchDB.plugin(require('pouchdb-adapter-asyncstorage').default)
const Settings = new PouchDB( "settings", { adapter: "asyncstorage" } );
const Account = new PouchDB( "account" , {adapter: "asyncstorage" } );

export {
	Settings,
	Account
}